-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Set 07, 2020 alle 21:42
-- Versione del server: 10.4.13-MariaDB
-- Versione PHP: 7.4.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tickethub`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(128) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `category`
--

INSERT INTO `category` (`category_id`, `name`, `description`, `disabled`) VALUES
(1, 'Teatro / Cabaret', 'Tutti gli spettacoli teatrali di vario genere.', 0),
(2, 'Fiere / Varie', 'Fiere ed eventi di vario genere.', 0),
(3, 'Concerti', 'Eventi riguardanti i concerti musicali delle band o altro.', 0),
(4, 'Cinema', 'Eventi riguardanti la proiezione di film.', 0),
(5, 'Ballo / Disco', 'Eventi riguardanti serate di ballo o di discoteca.', 0),
(6, 'Sport', 'Tutti gli eventi riguardanti le manifestazioni sportive.', 0),
(7, 'Mostre / Musei', 'Tutti gli eventi e conferenze riguardanti le mostre  e visite guidate.', 0),
(8, 'Parchi', 'Tutti gli eventi all\'aria aperta e nei parchi di divertimento.', 0),
(9, 'Categoria errore 404', 'Una categoria errata.', 1),
(15, 'test', 'test', 1),
(16, 'test2', 'yrs', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `event`
--

CREATE TABLE `event` (
  `event_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `seats` int(11) NOT NULL,
  `priority_seats` int(11) NOT NULL,
  `price` float NOT NULL,
  `input_date` datetime NOT NULL DEFAULT current_timestamp(),
  `starts_at` datetime NOT NULL,
  `ends_at` datetime NOT NULL,
  `address` varchar(256) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `category_id` int(11) NOT NULL,
  `organization_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `event`
--

INSERT INTO `event` (`event_id`, `name`, `image`, `description`, `seats`, `priority_seats`, `price`, `input_date`, `starts_at`, `ends_at`, `address`, `disabled`, `category_id`, `organization_id`) VALUES
(3, 'Visita Fortezza di Sarzanello', 'fortezza_sarzanello_320.jpg', 'La prima notizia certa dell’esistenza di un insediamento sul colle di Sarzanello risale al 963 d.C. quando l’imperatore Ottone I di Sassonia riconosce ad Adalberto Vescovo di Luni il possesso del Castrum Sarzanae, un sito fortificato situato nel luogo dove oggi si trova la fortezza.\r\nLa posizione strategica, dominante la Val di Magra, non poteva rimanere sprovvista di un presidio per il controllo di un’importante via di comunicazione tra nord e sud Italia.\r\nCon la vittoria nella guerra di Sarzana, resa dei conti delle aspirazioni fiorentine e genovesi, fu Lorenzo de’ Medici a ordinare la costruzione della Fortezza nella sua attuale forma.\r\nI lavori, iniziati nel 1493, vennero terminati nel 1502 dalla Repubblica di Genova sotto il cui controllo rimase fino all’unità d’Italia.\r\n\r\nOggi la Fortezza è aperta alle visite tutto l’anno e sede di eventi pubblici e privati.', 33, 0, 4.4, '2020-08-24 22:14:45', '2020-09-15 10:00:00', '2020-09-15 13:00:00', 'Fortezza di Sarzanello, Sarzana (SP)', 0, 7, 1),
(4, 'CI VEDIAMO AL GABBIANO!', 'logo_cinema_gabbiano_320.jpg', 'Multisala Gabbiano 1\r\nPer tutti i film a sorpresa del giovedì del Gabbiano', 55, 50, 22, '2020-08-24 22:29:10', '2020-09-18 21:00:00', '2020-09-18 23:30:00', 'Via Maierini 2, Senigallia (AN)', 0, 4, 1),
(5, 'Cronache di autodistruzione', 'barbascurax_320.jpg', 'Forse è un nostro diritto auto-distruggerci. Siamo o non siamo la specie più “intelligente” del pianta? Ma poi, intelligente in che senso?\r\n\r\nUn monologo ironico e delirante su alcune strane dinamiche della specie umana, tra ragioni evoluzionistiche e tanto sano impegno.\r\n\r\nAlla fine siamo animali, e non puoi dire animale senza dire MALE.', 297, 25, 11, '2020-08-24 22:42:20', '2020-08-24 21:30:00', '2020-08-24 23:00:00', ' San Mauro Pascoli (FC)  |  Corte Villa Torlonia ', 1, 1, 1),
(6, 'Aquadream', '1598905478-aquadream_320.jpg', 'Biglietti sconto per il parco acquatico Aquadream.', 400, 0, 27.5, '2020-08-24 22:42:20', '2020-09-24 10:30:00', '2020-09-24 23:00:00', 'Aquadream, 07021 Baja Sardinia (SS)', 0, 8, 1),
(8, 'MOTOR SHOW X-TREME', 'motor_show_xtreme_viareggio_320.jpg', ' Finalmente in Versilia l\'evento più spericolato dell\'anno: \"MOTOR SHOW X-TREME\" , lo spettacolo mozzafiato di esibizioni motoristiche eseguito da stuntman professionisti, specializzati in truck estremi, ed alla guida di auto su due ruote, eseguendo parcheggi tanto assurdi quanto incredibili. Un appuntamento imperdibile per gli appassionati dell\'azione e per le famiglie stesse che potranno ammirare a bocca aperta stunt riders con Ducati ed Honda, piloti acrobatici su prototipi quad ed effetti cinematografici stupefacenti, freestyle- bike trial ,testa coda con bmw et dulcis in fundo un\'esclusiva a sorpresa amata da grandi e piccini, TRANSFORMERS.\r\n\r\nNon solo uno spettacolo da ammirare, ma un\'esperienza da vivere in prima persona.  Lo show in questa fantastica edizione 2020, diventa ancora più interattivo: TAXI DRIFT -  Ai più coraggiosi verrà data la possibilità di salire in macchina con gli stuntman per vivere pienamente l’emozione del Motor Show.', 1496, 50, 12, '2020-08-24 22:45:44', '2020-09-18 08:30:00', '2020-09-18 19:30:00', 'Via Largo Risorgimento EX PAM, Viareggio (LU)', 0, 2, 1),
(9, 'BEATLES CONCERTO GROSSO', '1598885615-beatles-concerto-grosso_320.jpg', 'Replica di un concerto a sorpresa dei Beatles', 200, 20, 5, '2020-08-24 23:06:06', '2020-10-07 18:30:00', '2020-10-07 21:30:00', 'Palazzo Trinci (Corte), Foligno (PG)', 0, 3, 1),
(10, 'YPSIGROCK FESTIVAL 2021', 'Ypsigrock_Festival2021_320.jpg', 'Ypsigrock Festival è giunto alla sua 24esima edizione, che si terrà a Castelbuono, incantevole borgo medioevale in provincia di Palermo, dal 5 all’8 agosto 2021. I biglietti sono già disponibili alla sezione TICKETS.\r\nCome di consueto la prima giornata dell’evento, il giovedì (5 agosto) sarà dedicata all’apertura dell’area camping, con relativo Welcome Party ed eventi collaterali ad ingresso gratuito (mostre, djset, live etc). I live a pagamento (con cui accedere tramite 3-day pass o i day ticket) sono programmati dal 6 all’8 agosto, a partire dal tardo pomeriggio. I palchi sono 4 ma non suonano in contemporanea, quindi le singole esibizioni non si sovrappongono.', 2000, 400, 95.23, '2020-08-24 23:06:06', '2021-08-05 09:00:00', '2021-08-08 13:00:00', 'Castelbuono, Sicilia', 0, 3, 1),
(11, 'GP Emilia Romagna e Riviera di Rimini MISANO ADRIA', 'motogp-misano.jpg', 'Il Gran Premio di San Marino e di Rimini rappresenta un inedito esempio di collaborazione fra tre soggetti (Repubblica di San Marino, Provincia di Rimini e Misano World Circuit) che insieme producono la manifestazione.\r\nNon c’è tappa nel calendario della MotoGP che possa avvicinarsi a quella di Misano World Circuit quanto a radicamento della passione per i motori nel territorio circostante.\r\nIn tutte le classi ci sono piloti della zona,a partire ovviamente da Valentino Rossi i cui tifosi arrivano al circuito anche a piedi. Nel raggio di 50 km sono nati sportivamente campioni come Capirossi, Melandri, Dovizioso, Simoncelli ed altri ancora.\r\nDiverse scuderie fanno base in zona e in tantissime si parla romagnolo per la presenza di meccanici e tecnici. ', 1498, 296, 40, '2020-08-25 17:18:03', '2020-09-18 08:00:00', '2020-09-18 21:00:00', 'Via Daijiro Kato 10, 47843 MISANO ADRIATICO', 0, 6, 1),
(12, 'Max Pezzali', 'max-pezzali.jpg', 'Sul palco, Max Pezzali ripercorrerà, insieme ai fan, i più grandi successi della sua carriera trentennale, accompagnandoli in un viaggio indietro nel tempo, per rivivere le emozioni di un passato che soltanto lui sa come riportare alla luce.\r\n\r\nDa “Nord Sud Ovest Est” a “Come mai”, passando per “Sei un mito” e “Una canzone d’amore” fino ai successi più recenti: per una sola ed unica notte San Siro celebrerà i 30 anni di carriera di uno degli artisti più amati del nostro panorama, dando vita a quello che sarà un immenso karaoke. Uno spettacolo intenso in cui condivisione, allegria e anche un pizzico di nostalgia lasceranno il segno attraverso le canzoni che, almeno una volta nella vita, hanno rappresentato ognuno di noi.\r\n', 4000, 500, 57.5, '2020-08-25 17:31:24', '2021-06-26 21:00:00', '2021-06-27 00:00:00', 'Stadio Comunale, Via Timavo, 10, 30020 BIBIONE', 0, 3, 1),
(13, 'Ma quande lingues coalesce', 'tacchino.png', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2020-09-20 09:57:14', '2020-09-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(14, 'Ma che bella pagliacciata!', '1598885500-2121874609.png', 'Una mostra delle più famose pagliacciate. Che risate', 543, 43, 5.53, '2020-08-27 09:58:48', '2020-10-18 09:57:14', '2020-10-18 12:57:14', 'via del buffone 610', 0, 1, 1),
(15, 'Ma quande lingues coalesce', 'ticket.jpg', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2020-11-20 09:57:14', '2020-11-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(16, 'Un bel tendone rosso', '1598900059-fiera.png', 'Una bella piccola descrizione', 404, 101, 10.51, '2020-08-27 09:58:48', '2020-12-21 09:57:14', '2020-12-21 12:57:14', 'piazza pulita', 0, 2, 1),
(17, 'Ma quande lingues coalesce', 'tacchino.png', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2021-01-20 09:57:14', '2021-01-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(18, 'Ma quande lingues coalesce', 'ticket.jpg', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '0000-00-00 00:00:00', '2021-02-20 09:57:14', '2021-02-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(19, 'Ma quande lingues coalesce', 'clown.png', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2021-03-20 09:57:14', '2021-03-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(20, 'Ma quande lingues coalesce', 'fiera.png', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2021-04-20 09:57:14', '2021-04-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(21, 'Ma quande lingues coalesce', 'ticket.jpg', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2021-05-20 09:57:14', '2021-05-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(22, 'Ma quande lingues coalesce', 'tacchino.png', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2021-06-20 09:57:14', '2021-06-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(23, 'Ma quande lingues coalesce', 'clown.png', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2021-07-20 09:57:14', '2021-07-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(24, 'Ma quande lingues coalesce', 'tacchino.png', 'Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li nov lingua franca va esser plu simplic e regulari quam li existent Europan lingues. It va esser tam simplic quam Occidental: in fact, it va esser Occidental. A un Angleso it va semblar un simplificat Angles, quam un skeptic Cambridge amico dit me que Occidental es.Ma quande lingues coalesce, li grammatica del resultant lingue es plu simplic e regulari quam ti del coalescent lingues. Li', 40, 10, 10.5, '2020-08-27 09:58:48', '2021-08-20 09:57:14', '2021-08-20 12:57:14', 'It va esser tam simplic quam Occidental', 0, 8, 1),
(38, 'The Luca Giurato Show', '1598905228-il_meglio_di_me_stessa.png', 'Lo show di Luca Giurato', 46, 0, 1, '2020-08-29 19:01:33', '2020-09-08 20:00:00', '2020-09-09 00:00:00', 'Rai 1', 0, 15, 3),
(40, 'Fiera della probabilità', '1598819649-dado2.png', 'Questa è la fiera delle probabilità magiche', 250, 10, 0, '2020-08-30 22:34:09', '2020-09-21 09:00:00', '2020-09-21 20:00:00', 'improbabile', 0, 2, 1),
(41, 'Fiera della probabilità', '1598819694-dado2.png', 'Questa è la fiera delle probabilità magiche', 250, 10, 0, '2020-08-30 22:34:54', '2020-10-21 09:00:00', '2020-10-21 20:00:00', 'improbabile', 0, 2, 1),
(42, 'Fiera della probabilità', '1598819744-dado2.png', 'Questa è la fiera delle probabilità magiche', 250, 10, 0, '2020-08-30 22:35:44', '2020-12-21 09:00:00', '2020-12-21 20:00:00', 'improbabile', 0, 2, 1),
(43, 'Fiera della probabilità', '1598819796-dado2.png', 'Questa è la fiera delle probabilità magiche.', 0, 0, 0.01, '2020-08-30 22:36:36', '2020-09-12 09:00:00', '2020-09-12 20:00:00', 'improbabile', 0, 2, 1),
(44, 'si volaaaaa', '1599164280-EaYgEIEXYAEmq1T_lasrge.jpg', 'lancio', 121, 13, 1, '2020-09-03 22:18:00', '2020-09-16 12:00:00', '2020-09-16 18:00:00', 'marte', 0, 4, 3),
(45, 'Fiera di Lunacupa', '1599503105-338371.jpg', 'La Fiera di Lunacupa è in città una settimana al ogni mese, con cimeli di famiglia, giocattoli spettrali, battaglia mascotte, e gli set per la trasmogrificazione per acquisire!', 1336, 1337, 10, '2020-09-06 20:25:05', '2020-09-08 00:00:00', '2020-10-08 00:00:00', 'Isola di Lunacupa', 0, 2, 3);

-- --------------------------------------------------------

--
-- Struttura della tabella `organization`
--

CREATE TABLE `organization` (
  `organization_id` int(11) NOT NULL,
  `founder_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `description` varchar(256) NOT NULL,
  `address` varchar(80) NOT NULL,
  `disabled` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `organization`
--

INSERT INTO `organization` (`organization_id`, `founder_id`, `name`, `description`, `address`, `disabled`) VALUES
(1, 9, 'ACME', 'A Company Making Everything', 'via Cesare Pavese 50, Cesena (FC) 47521', 0),
(3, 12, 'Organizzazione di Claudio', 'Questa è un organizzazione no profit', 'prato', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `ticket`
--

CREATE TABLE `ticket` (
  `ticket_id` int(11) NOT NULL,
  `validated` datetime DEFAULT NULL,
  `code` varchar(50) NOT NULL,
  `price` float NOT NULL,
  `priority_seat` tinyint(1) NOT NULL DEFAULT 0,
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `ticket`
--

INSERT INTO `ticket` (`ticket_id`, `validated`, `code`, `price`, `priority_seat`, `disabled`, `event_id`, `user_id`) VALUES
(470, NULL, '3-66027006', 4.4, 1, 0, 3, 12),
(471, '2020-09-05 19:00:21', '8-b1424de3', 12, 0, 0, 8, 12),
(472, '2020-09-06 13:36:24', '38-3b916a95', 1, 0, 0, 38, 12),
(474, NULL, '3-7d230fa0', 4.4, 0, 0, 3, 1),
(489, NULL, '11-27c0b622', 40, 0, 0, 11, 12),
(492, NULL, '11-68fef56b', 40, 0, 0, 11, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `surname` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `role` set('ADM','ORG','USR') NOT NULL DEFAULT 'USR',
  `disabled` tinyint(1) NOT NULL DEFAULT 0,
  `organization_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `user`
--

INSERT INTO `user` (`user_id`, `name`, `surname`, `email`, `password`, `salt`, `role`, `disabled`, `organization_id`) VALUES
(1, 'Sauro', 'Camagni', 'sauro.camagni@studio.unibo.it', '84bee5d237ddb4e8d2d5351f1397e225a2f000a135673eab84d05091645fed368cb402f9d3113ce4a21c5621b734aa4f2238ff06b17f083cf1cd91cd0b545bdd', 'babc6c156d36968cef18aa468fac19872240ff9e9b4260975ad380db66545bdc138c5dd4c7850f40fc453c02b2dd8814b2bb0931e8dfa9e114178739c1b962e2', 'ADM', 0, NULL),
(9, 'Organiz', 'Organiz', 'org@org.it', '1a762ebe42b79d73d8620e7a3e3c9f88b304288246beadaf0b0ebbc31d9ac6900ed6bb6fd78fbc92f9d6c6a769d0a736687d5a43a239ffbcdf8a3e18bab874ed', 'cff2fe79bf0f3a2ba17f6119872f9f99b448ff31ae5bf8f700d5a5f28d064dd8aefaca83167a3f593950f3b59c6b76cb3d778390ba496834fd4ead022ff58efd', 'ORG', 0, 1),
(10, 'Utente', 'Utente', 'usr@usr.it', '255ef0ed24b963662276e749b9143fcb3ca4c887c53315cf3f3d33fad437f144cb89743e18d14e3bf4f0ab166807f51faa728df1e09ad9a1b6ece9ec38d4007d', '4369b947d0927b464d802929b2fe7e37bef619b535fad7aad2ecba998d1a71d7ee2c54a6e00bc9391454491ff13f303379c93da0b8913117325d12cebba78542', 'USR', 0, NULL),
(12, 'Claudio', 'Bisio', 'claudio@bisio.it', '2c21e932122b21e5c5676482e7a51d24a251a748d0e57e76e292c2a748f9789d2a930d9060926782fa0968107acf5baf64fb207877ae867c2fa6a7e5b137a3d9', 'c3215aa5725ba6876d0b11007327b93643ae54a6ba95e00801f7d6e519fbcdb63bb099589c8ea7cbe9429dd7b0528f93ca73cea9032af8f10b825a5e29542ee4', 'ORG', 0, 3),
(18, 'test', 'cognome', 'test@test.it', 'cac21bc30d2139bae48e55376c36a928ebd59e36885380a7b8271a32c788f41e361cf20b4adc79e658386a977944741b0351b19a3e0a60228ce8dea074f3e375', 'cb1b40c6fea65bff3914fefe60d01aa08c6547e132f9a92bb90565a917a5864c5ebfefac3c12d26ff70a2bcd1cacaa980f14bb18f370f89cfb0280de8a0eed24', 'USR', 1, NULL),
(20, 'testone', 'testone', 'testone@test.it', '2b77cd85efee4c1c007b95deb2f3860b96a7d57319cfeb109e77ad64dfe70e25b3d644e9cfef3e19516805047405e46691aebc5a449cce14c082accc2b7ffdf1', '29ec3805965a6a352fa0e8f34cd701aa50dc4bb4485a15d8eac65f045e826acb6be56595d15ffd3744a448ef915ee641aec2d42b4fff8f99cb2631ed16d561ed', 'ADM', 0, NULL);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`) USING BTREE,
  ADD UNIQUE KEY `name` (`name`);

--
-- Indici per le tabelle `event`
--
ALTER TABLE `event`
  ADD PRIMARY KEY (`event_id`),
  ADD KEY `organization_ibfk_1` (`organization_id`),
  ADD KEY `category_ibfk_1` (`category_id`);

--
-- Indici per le tabelle `organization`
--
ALTER TABLE `organization`
  ADD PRIMARY KEY (`organization_id`) USING BTREE,
  ADD UNIQUE KEY `founder_id` (`founder_id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indici per le tabelle `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ticket_id`),
  ADD UNIQUE KEY `code` (`code`),
  ADD KEY `event_ibfk_1` (`event_id`),
  ADD KEY `user_ibfk_1` (`user_id`);

--
-- Indici per le tabelle `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `fk_organization_id` (`organization_id`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT per la tabella `event`
--
ALTER TABLE `event`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- AUTO_INCREMENT per la tabella `organization`
--
ALTER TABLE `organization`
  MODIFY `organization_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT per la tabella `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=496;

--
-- AUTO_INCREMENT per la tabella `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `category_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`),
  ADD CONSTRAINT `organization_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`organization_id`);

--
-- Limiti per la tabella `organization`
--
ALTER TABLE `organization`
  ADD CONSTRAINT `founder_ibfk_1` FOREIGN KEY (`founder_id`) REFERENCES `user` (`user_id`);

--
-- Limiti per la tabella `ticket`
--
ALTER TABLE `ticket`
  ADD CONSTRAINT `event_ibfk_1` FOREIGN KEY (`event_id`) REFERENCES `event` (`event_id`),
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Limiti per la tabella `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `fk_organization_id` FOREIGN KEY (`organization_id`) REFERENCES `organization` (`organization_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
