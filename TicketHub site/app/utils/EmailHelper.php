<?php

class EmailHelper
{
    // carriage return type (RFC)
    public const EOL = "\r\n";

    public $subject = "Oggetto non specificato";

    // a random hash will be necessary to send mixed content
    private  $separator;

    public  $fromName;
    public  $fromEmail;

    private $headers;


    public function __construct($fronName, $fromEmail, $subject = "Oggetto non specificato")
    {
        $this->fronName = $fronName;
        $this->fromEmail = $fromEmail;
        $this->subject = $subject;
    }


    public function send($message, $mailto)
    {
        $this->headers = $this->generateHeader();

        $body = $this->buildMessage($message);
        $body .= $this->endMessage();

        if (mail($mailto, $this->subject, $body, $this->headers)) {
            //email inviata con successo
            return true;
        } else {
            //c'è stato un errore durante l'invio della e-mail
            // print_r(error_get_last());
            return false;
        }
    }

    public function sendWithAttached($message, $path, $filename, $mailto)
    {
        $this->headers = $this->generateHeader();

        $body = $this->buildMessage($message);
        $body .= $this->buildAttached($path, $filename);
        $body .= $this->endMessage();

        if (mail($mailto, $this->subject, $body, $this->headers)) {
            //email inviata con successo
            return true;
        } else {
            //c'è stato un errore durante l'invio della e-mail
            // print_r(error_get_last());
            return false;
        }
    }

    //genera l'header della e-mail
    private function generateHeader()
    {
        $this->separator = md5(time());

        // main header (multipart mandatory)
        $headers = "From: $this->fromName <$this->fromEmail>" . self::EOL;
        $headers .= "MIME-Version: 1.0" . self::EOL;
        $headers .= "Content-Type: multipart/mixed; boundary=\"" . $this->separator . "\"" . self::EOL;
        $headers .= "Content-Transfer-Encoding: 7bit" . self::EOL;
        $headers .= "This is a MIME encoded message." . self::EOL;

        return $headers;
    }

    //costruisce il messaggio di testo
    private function buildMessage($message)
    {
        // message
        $body = "--" . $this->separator . self::EOL;
        $body .= "Content-Type: text/plain; charset=\"utf-8\"" . self::EOL; //provare con utf-8: charset=\"iso-8859-1\""
        $body .= "Content-Transfer-Encoding: 8bit" . self::EOL;
        $body .= self::EOL . $message . self::EOL . self::EOL;

        return $body;
    }

    //costruisce il messaggio con l'allegato
    private function buildAttached($path, $filename)
    {
        $file = $path . $filename;

        $content = file_get_contents($file);
        $content = chunk_split(base64_encode($content));

        // attachment
        $body = "--" . $this->separator . self::EOL;
        $body .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"" . self::EOL;
        $body .= "Content-Transfer-Encoding: base64" . self::EOL;
        $body .= "Content-Disposition: attachment" . self::EOL;
        $body .= self::EOL . $content . self::EOL . self::EOL;

        return $body;
    }

    //l'ultima linea della email
    private function endMessage()
    {
        return  "--" . $this->separator . "--";
    }
}
