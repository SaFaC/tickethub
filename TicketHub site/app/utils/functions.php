<?php

function stringToBoolean($str)
{
    return filter_var($str, FILTER_VALIDATE_BOOLEAN);
}

function sec_session_start()
{
    $session_name = 'sec_session_id'; // Imposta un nome di sessione
    $secure = false; // Imposta il parametro a true se vuoi usare il protocollo 'https'.
    $httponly = true; // Questo impedirà ad un javascript di essere in grado di accedere all'id di sessione.
    ini_set('session.use_only_cookies', 1); // Forza la sessione ad utilizzare solo i cookie.
    $cookieParams = session_get_cookie_params(); // Legge i parametri correnti relativi ai cookie.
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    session_name($session_name); // Imposta il nome di sessione con quello prescelto all'inizio della funzione.
    session_start(); // Avvia la sessione php.
    session_regenerate_id(); // Rigenera la sessione e cancella quella creata in precedenza.
}

function login_check($app)
{
    $login = false;

    // Verifica che tutte le variabili di sessione siano impostate correttamente
    if (isset($_SESSION['user_id'], $_SESSION['name'], $_SESSION['login_string'])) {
        $user_id = $_SESSION['user_id'];
        $login_string_session = $_SESSION['login_string'];
        // $name = $_SESSION['name'];
        $user_browser = $_SERVER['HTTP_USER_AGENT']; // reperisce la stringa 'user-agent' dell'utente.

        //carica il file che si occupa degli utenti
        require_once ROOT . "/models/User.php";
        $userDB = new User($app);


        $user = $userDB->getUserById($user_id)["data"];

        if ($user) { // se l'utente esiste

            $passwordDB = $user["password"]; // recupera la password dal risultato ottenuto.

            $login_string_check = hash('sha512', $passwordDB . $user_browser);

            //se la password del db è la stessa di quella presente in session, allora l'utente è già loggato
            if ($login_string_check == $login_string_session) {
                // Login eseguito!!!!
                $login = true;
            }
        }
    }

    if (!$login) {
        //utente non riconosciuto, viene salvato solo il ruolo di ospite
        $user["role"] = "GST";
    }

    if (DEBUG_ON) {
        //se è attiva la modalità debug, il ruolo viene sovrascritto
        $user["role"] =  DEBUG_USER;
    }

    return $user;
}

function createQR($code)
{

    // Initialize a file URL to the variable 
    $url = "https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=$code";

    // Initialize the cURL session 
    $ch = curl_init($url);


    // Use basename() function to return 
    // the base name of file  
    $file_name = "$code.png"; //basename($url); 

    // Save file into file location 
    $save_file_loc = ROOT . "/.." . QR_TICKETS . $file_name;

    // Open file  
    $fp = fopen($save_file_loc, 'wb');

    // It set an option for a cURL transfer 
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // Perform a cURL session 
    curl_exec($ch);

    // Closes a cURL session and frees all resources 
    curl_close($ch);

    // Close file 
    fclose($fp);
}

function printvar($data){
    echo "<pre>";
    var_dump($data);
    die();
}