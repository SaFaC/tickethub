<?php

function createQR($code)
{

    // Initialize a file URL to the variable 
    $url = "https://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=$code";

    // Initialize the cURL session 
    $ch = curl_init($url);


    // Use basename() function to return 
    // the base name of file  
    $file_name = "$code.png"; //basename($url); 

    // Save file into file location 
    $save_file_loc = ROOT . "/.." . QR_TICKETS . $file_name;

    // Open file  
    $fp = fopen($save_file_loc, 'wb');

    // It set an option for a cURL transfer 
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);

    // Perform a cURL session 
    curl_exec($ch);

    // Closes a cURL session and frees all resources 
    curl_close($ch);

    // Close file 
    fclose($fp);
}
