<?php
require_once ROOT . "/utils/EmailHelper.php";


class EmailNotifier extends EmailHelper
{
    private $app;

    function __construct()
    {
        $this->app = $GLOBALS["app"];

        parent::__construct(
            "TicketHub",
            "no-reply@tickethub.it"
        );
    }

    //notifica gli utenti che hanno acquistato un biglietto di eventuali cambiamenti agli evendi dei biglietti
    public function notifyTheChanges($originalEvent, $modifiedevent)
    {
        //memorizza se ci sono modifiche da inviare
        $change = false;

        //imposta il soggetto della e-mail
        $this->subject = "TicketHub - Modifiche ad un evento da te prenotato.";

        //carica il file che si occupa dei ticket
        require_once ROOT . "/models/Ticket.php";
        $ticketDB = new Ticket($this->app);

        //elenco di utenti a cui notificare le modifiche
        $users = $ticketDB->getUsersByEvent($originalEvent["event_id"])["data"];

        if (!$users) {
            //non ci sono utenti da notificare
            $change = false;
        } else {
            //notifica via e-mail: preparazione del messaggio

            $message = "Ti contattiamo per avvisarti che sono state apportate delle modifiche ad un evento a cui hai acquistato un biglietto." . self::EOL;
            $message .= "L'evento in questione è" . self::EOL . "{$originalEvent["name"]}." . self::EOL;
            $message .= "Qui di seguito troverai elencate le modifiche che sono state effettuate." . self::EOL;
            $message .= "In caso di problemi non esitare a contattarci all'indirizzo e-mail support@tickethub.it" . self::EOL;
            $message .= "(Questa è una e-mail auto-generata, non rispondere a questo indirizzo email; le email ricevute su no-replay@tickethub.it vengono automaticamente cestinate)" . self::EOL . self::EOL;

            //foreach che scorre tutte le modifiche effettuate e le aggiunge al testo
            foreach ($modifiedevent as $key => $value) {
                $finally = true;
                switch ($key) {
                    case "name":
                        $message .= "Il titolo è stato cambiato." . self::EOL;
                        $message .= "Prima: {$originalEvent["name"]}" . self::EOL;
                        $message .= "Dopo: $value" . self::EOL;
                        break;

                    case "image":
                        $message .= "L'immagine è stata cambiata." . self::EOL;
                        break;

                    case "category_id":
                        //carica il file che si occupa delle categorie
                        require_once ROOT . "/models/Category.php";
                        $categoryDB = new Category($this->app);
                        $originaCategory = $categoryDB->getCategoryById($originalEvent["category_id"])["data"];
                        $changedCategory = $categoryDB->getCategoryById($value)["data"];

                        $message .= "La categoria è stata cambiata." . self::EOL;
                        $message .= "Prima: {$originaCategory["name"]}" . self::EOL;
                        $message .= "Dopo: {$changedCategory["name"]}" . self::EOL;
                        break;

                    case "description":
                        $message .= "La descrizioneè stata cambiata." . self::EOL;
                        $message .= "Prima: {$originalEvent["description"]}" . self::EOL;
                        $message .= "Dopo: $value" . self::EOL;
                        break;

                    case "address":
                        $message .= "L'indirizzo del luogo è stato cambiato." . self::EOL;
                        $message .= "Prima: {$originalEvent["address"]}" . self::EOL;
                        $message .= "Dopo: $value" . self::EOL;
                        break;

                    case "price":
                        $message .= "Il prezzo è stato cambiato." . self::EOL;
                        $message .= "Prima: €{$originalEvent["price"]}" . self::EOL;
                        $message .= "Dopo: €$value" . self::EOL;
                        break;

                    case "starts_at":
                        $message .= "La data di inizio è stata cambiata." . self::EOL;
                        $message .= "Prima: {$originalEvent["starts_at"]}" . self::EOL;
                        $message .= "Dopo: $value" . self::EOL;
                        break;

                    case "ends_at":
                        $message .= "La data di fine è stata cambiata." . self::EOL;
                        $message .= "Prima: {$originalEvent["ends_at"]}" . self::EOL;
                        $message .= "Dopo: $value" . self::EOL;
                        break;

                    case "disabled":
                        $message .= "L'evento è stato " . ($value ? "annullato" : "ripristinato") . "" . self::EOL;
                        break;

                    default:
                        $finally = false;
                        break;
                }
                //in ogni caso dello switch esegue l'if (tranne il default)
                if ($finally) {
                    $message .= "" . self::EOL;
                    $change = true;
                }
            }

            //c'è stao un cambiamento da notificare, invia la e-mail
            if ($change) {
                //foreach che scorre tra tutti gli utenti e invia le e-mail
                foreach ($users as $user) {

                    $finalMessage = "Ciao {$user["name"]} {$user["surname"]}" . self::EOL . self::EOL . $message;

                    $this->send($finalMessage, $user["email"]);
                }
            }
        }
        return $change;
    }

    //invia il biglietto via e-mail
    public function sendTicket($ticket, $event)
    {
        //imposta il soggetto della e-mail
        $this->subject = "TicketHub - Acquisto biglietto evento: \"{$event["name"]}\"";

        $message = "Ciao {$this->app->globalUser["name"]} {$this->app->globalUser["surname"]}" . self::EOL . self::EOL;
        $message .= "Grazie per aver acquistato dal nostro store." . self::EOL;
        $message .= "Questa è la email di conferma dell'acquisto del tuo biglietto virtuale per l'evento:" . self::EOL;
        $message .= $event["name"] . self::EOL;
        $message .= "In allegato troverai codice digitale del biglietto: " .
            "quando ti recherai all'evento un operatore ti chiederà mi mostrarlo. In caso si dovessero presentare dei problemi, il codice del biglietto è:" . self::EOL;
        $message .= $ticket["code"] . self::EOL;
        $message .= "In caso di problemi non esitare a contattarci all'indirizzo e-mail support@tickethub.it" . self::EOL;
        $message .= "(Questa è una e-mail auto-generata, non rispondere a questo indirizzo email; le email ricevute su no-replay@tickethub.it vengono automaticamente cestinate)" . self::EOL . self::EOL;

        $this->sendWithAttached(
            $message,
            ROOT . "/.." . QR_TICKETS,
            $ticket["code"] . ".png",
            $this->app->globalUser["email"]
        );
    }

    //notifica il creatore dell'organizzazione dell'esaurimento posti dell'evento
    public function notifySoldOut($event)
    {

        //carica il file che si occupa delle organizzazioni
        require_once ROOT . "/models/Organization.php";
        $orgDB = new Organization($this->app);

        $data = $orgDB->getFounder($event["organization_id"]);
        if (!$data) {
            //il fondatore non esiste, qualcosa non va nel db...
        } else {
            $founder = $data["data"];

            //imposta il soggetto della e-mail
            $this->subject = "TicketHub: evento sold out: \"{$event["name"]}\"";

            $message = "Ciao {$founder["name"]} {$founder["surname"]}" . self::EOL . self::EOL;
            $message .= "Ti contattiamo per informarti dell'esaurimento dei posti dell'evento \"{$event["name"]}\" organizzato dalla tua organizzazione" . self::EOL;
            $message .= "In caso di problemi non esitare a contattarci all'indirizzo e-mail support@tickethub.it" . self::EOL;
            $message .= "(Questa è una e-mail auto-generata, non rispondere a questo indirizzo email; le email ricevute su no-replay@tickethub.it vengono automaticamente cestinate)" . self::EOL . self::EOL;

            $this->send($message, $founder["email"]);
        }
    }
}
