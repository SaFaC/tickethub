<?php

class DatabaseHelper
{
    private $pdo;

    public function __construct($servername, $username, $password, $dbname)
    {
        //crea un oggetto di tipo PDO per connettere PHP e il database
        try {
            $this->pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION); //tutti i warning e errori generano eccezione
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); //permette di non dover indicare il tipo durante il bind dei parametri nelle Prepared statements query 
        } catch (Throwable $th) {
            //c'è stato un errore di connessione, chiude il programma
            die("Impossibile connettersi al database, contattare l'amministratore del sistema.");
        }
        
    }

    //Metodo che esegue la query e ne restituisce i risultati
    public function select($query, $paramsQuery = [])
    {
        $stmt = $this->pdo->prepare($query);
        $stmt->execute($paramsQuery);

        $res = $stmt->fetchAll();
        return $res;
    }

    //Metodo che inserisce una riga
    //se ha avuto successo, restituisce l'id dell'ultima riga inserita
    //se ha fallito l'inserimento, restituisce false
    public function insert($query, $paramsQuery)
    {
        $stmt = $this->pdo->prepare($query);
        $success = $stmt->execute($paramsQuery);

        return $success ? $this->pdo->lastInsertId() : false;
    }

    //Metoto che esegue l'update
    //restituisce true o false a seconda se ha successo o no
    public function update($query, $paramsQuery)
    {
        $stmt = $this->pdo->prepare($query);
        $success = $stmt->execute($paramsQuery);

        return $success;
    }
}