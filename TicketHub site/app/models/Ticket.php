<?php

class Ticket extends BaseModel
{
    function __construct($app)
    {
        parent::__construct($app, "ticket");
    }

    public function getUsersByEvent($event_id)
    {
        $query =    "SELECT DISTINCT u.name, u.surname, u.email, u.role
                    FROM user u, ticket t
                    WHERE t.disabled = 0 AND u.disabled = 0
                    AND t.event_id = ?
                    AND t.user_id = u.user_id 
        ";
        $data = $this->dbh->select($query, [$event_id]);

        return
            [
                "data" => empty($data) ? false : $data
            ];
    }

    public function createTicket($data)
    {
        $res = $this->insert($data);
        return $res ? true : false;
    }

    //restituisce l'utente del biglietto
    public function getVerboseTicket($ticket_id)
    {
        $query =     "SELECT u.name, u.surname, u.email, t.ticket_id, t.code, t.validated, t.price, t.priority_seat
                    FROM ticket t, user u
                    WHERE t.code = ?
                    AND t.user_id = u.user_id";
        $data = $this->dbh->select($query, [$ticket_id]);

        return
            [
                "data" => empty($data) ? false : $data[0]
            ];
    }

    //valida il biglietto con la data attuale e restituisce il successo
    public function validateTicket($ticket_id)
    {
        $data = $this->update($ticket_id, ["validated" => date("Y-m-d H:i:s")]);
        return
            [
                "success" => !!$data
            ];
    }
}
