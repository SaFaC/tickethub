<?php

class Authentication extends BaseModel
{
    function __construct($app)
    {
        parent::__construct($app, "user");
    }

    //restituisce false se ha fallito l'inserimento, altrimenti i dati dell'utente inserito
    public function userRegister($name, $surname, $email, $password, $random_salt)
    {

        // "INSERT INTO user (name, surname, email, password, salt) VALUES (?, ?, ?, ?, ?)"
        $data = [
            "name" => $name,
            "surname" => $surname,
            "email" => $email,
            "password" => $password,
            "salt" => $random_salt
        ];

        return $this->insert($data);
    }

    //restituisce o meno l'account tramite indirizzo e-mail (è univoco come l'id)
    public function getUserByEmail($email)
    {
        $query =    "SELECT *
                    FROM user 
                    WHERE email = ? 
                    LIMIT 1";
        $data = $this->dbh->select($query, [$email]);

        return empty($data) ? false : $data[0];
    }

}
