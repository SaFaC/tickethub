<?php

class Organization extends BaseModel
{
    function __construct($app)
    {
        parent::__construct($app, "organization");
    }

    public function getFounder($organization_id)
    {
        $query =    "SELECT u.*
                    FROM  organization o, user u
                    WHERE o.organization_id  = ?
                    AND o.founder_id = u.user_id";
        $data = $this->dbh->select($query, [$organization_id]);

        return [
            "data" => empty($data) ? false : $data[0]
        ];
    }

    //query per l'admin, restituisce completamente tutta la tabella organization
    public function getAdmOrganizations()
    {
        $query =    "SELECT o.*, u.name as founder_name
                        FROM organization o, user u
                        WHERE o.founder_id = u.user_id";
        $data = $this->dbh->select($query);

        return [
            "data" => $data
        ];
    }


    public function getOrganizationById($organization_id)
    {
        $data = $this->selectForId($organization_id);

        return [
            "data" => $data
        ];
    }


    public function getOrganizations($disabled = 0)
    {
        $query =    "SELECT *
                    FROM organization 
                    WHERE disabled = ?";
        $data = $this->dbh->select($query, [$disabled]);

        return [
            "data" => $data
        ];
    }
}
