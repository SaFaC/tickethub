<?php

class User extends BaseModel
{
    function __construct($app)
    {
        parent::__construct($app, "user");
    }

    //restituisce o meno l'account traminte l'id
    public function getUserById($user_id)
    {
        $data = $this->selectForId($user_id);

        return [
            "data" => $data
        ];
    }

    //query per l'admin, restituisce completamente tutta la tabella users
    public function getAdmUsers()
    {
        $query =    "SELECT *
                        FROM user";
        $data = $this->dbh->select($query);

        return [
            "data" => $data
        ];
    }

    //esegue l'ìupdate di una categoria
    public function updateUser($userid, $updateData)
    {
        $res = $this->update($userid, $updateData);
        return $res ? $res : false;
    }
}
