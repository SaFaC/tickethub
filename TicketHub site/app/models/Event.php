<?php

class Event extends BaseModel
{
    public function __construct($app)
    {
        parent::__construct($app, "event");
    }

    //restituisce un array associativo con al massimo 6 eventi e in fondo un array associativo 
    //di un elemento contenente il totale ("tot") di eventi visualizzabili
    public function selectHomeEvents($page = 1)
    {
        return $this->selectSearchHomeEvents($page, "");
    }

    public function selectSearchHomeEvents($page = 1, $title = "")
    {
        $limitPerPage = 6;
        $title = "%" . $title . "%"; //aggiunta carattere jolly per la ricerca
        //estrapola gli eventi a seconda della pagina richiesta
        $page = max(1, abs($page));
        $offset = (($page * $limitPerPage) - $limitPerPage);
        $query = "SELECT event_id, name, image, IF(CHAR_LENGTH(description) >= 115, CONCAT(SUBSTR(description , 1, 112), '...'), description) AS description, price 
                FROM event 
                WHERE disabled = 0 AND ends_at >= CURRENT_TIMESTAMP()
                AND LOWER(name) LIKE LOWER(?)
                ORDER BY starts_at
                LIMIT $offset, $limitPerPage";

        $data = $this->dbh->select($query, [$title]);

        //estrapola quanti eventi potenzialmente potrebbe visualizzare
        $query = "SELECT COUNT(event_id) AS tot 
        FROM event 
        WHERE disabled = 0 AND ends_at >= CURRENT_TIMESTAMP()
        AND LOWER(name) LIKE LOWER(?)";
        $tot = $this->dbh->select($query, [$title])[0];

        return [
            "data" => $data,
            "skip" => $offset,
            "limit" => $limitPerPage,
            "total" => $tot["tot"],
        ];
    }

    public function selectSearchLastAddedEvents($page = 1, $title = "")
    {
        $limitPerPage = 6;
        $title = "%" . $title . "%"; //aggiunta carattere jolly per la ricerca
        //estrapola gli eventi a seconda della pagina richiesta
        $page = max(1, abs($page));
        $offset = (($page * $limitPerPage) - $limitPerPage);
        $query = "SELECT event_id, name, image, IF(CHAR_LENGTH(description) >= 115, CONCAT(SUBSTR(description , 1, 112), '...'), description) AS description, price 
                FROM event 
                WHERE disabled = 0 AND ends_at >= CURRENT_TIMESTAMP()
                AND LOWER(name) LIKE LOWER(?)
                AND DATEDIFF(CURRENT_TIMESTAMP(), input_date) <= 7
                ORDER BY input_date DESC
                LIMIT $offset, $limitPerPage";

        $data = $this->dbh->select($query, [$title]);

        //estrapola quanti eventi potenzialmente potrebbe visualizzare
        $query = "SELECT COUNT(event_id) AS tot 
        FROM event 
        WHERE disabled = 0 AND ends_at >= CURRENT_TIMESTAMP()
        AND LOWER(name) LIKE LOWER(?)
        AND DATEDIFF(CURRENT_TIMESTAMP(), input_date) <= 7";
        
        $tot = $this->dbh->select($query, [$title])[0];

        return [
            "data" => $data,
            "skip" => $offset,
            "limit" => $limitPerPage,
            "total" => $tot["tot"],
        ];
    }

    public function getEventById($event_id)
    {
        $query =    "SELECT event.*, event.name as title, category.name as category
                    , (TIMEDIFF(event.ends_at, event.starts_at))as duration
                    FROM event, category 
                    WHERE event_id = ? 
                    AND event.category_id = category.category_id";
        $data = $this->dbh->select($query, [$event_id]);
        $data = empty($data) ? false : $data;
        return [
            "data" => $data[0]
        ];
    }

    //query per l'admin, restituisce completamente tutta la tabella events
    public function getAdmEvents()
    {
        $query =    "SELECT e.*, o.name as organization_name
                        FROM event e, organization o
                        WHERE e.organization_id = o.organization_id";
        $data = $this->dbh->select($query);

        return [
            "data" => $data
        ];
    }

    //query per l'admin, restituisce completamente tutta la tabella events
    public function getOrgEvents($manager_id)
    {
        $query =    "SELECT DISTINCT e.*, o.name as organization_name
                        FROM event e, organization o, user u
                        WHERE e.organization_id = o.organization_id
                        AND e.organization_id = ?";
        $data = $this->dbh->select($query, [$manager_id]);

        return [
            "data" => $data
        ];
    }

    public function insertEvent($event)
    {
        $res = $this->insert($event);
        return $res ? $res : false;
    }

    public function updateEvent($eventId, $updateData)
    {
        $res = $this->update($eventId, $updateData);
        return $res ? $res : false;
    }

    public function isOwner($user_id, $event_id)
    {
        $query =    "SELECT u.user_id
                    FROM organization o, event e, user u
                    WHERE u.user_id = ? AND e.event_id = ?
                    AND e.organization_id = o.organization_id
                    AND o.organization_id = u.organization_id
                    AND u.role = 'ORG'";
        $data = $this->dbh->select($query, [$user_id, $event_id]);

        return empty($data) ? false : true;
    }
}
