<?php

class Category extends BaseModel
{
    function __construct($app)
    {
        parent::__construct($app, "category");
    }

    //restituisce tutte le categorie
    public function getCategories($disabled = 0)
    {

        $query =    "SELECT *
                    FROM category
                    WHERE disabled = ?
                ";
        $data = $this->dbh->select($query, [$disabled]);

        return [
            "data" => $data
        ];
    }

    //restituisce una categoria per id
    public function getCategoryById($category_id)
    {
        $data = $this->selectForId($category_id);
        return [
            "data" => $data
        ];
    }

    //query per l'admin, restituisce completamente tutta la tabella caregory
    public function getAdmCategories()
    {
        $query =    "SELECT *
                    FROM category";
        $data = $this->dbh->select($query);

        return [
            "data" => $data
        ];
    }

    //verifica l'esistenza di una catagoria
    public function existCategoryByName($categoryName)
    {
        $query =    "SELECT *
                    FROM category
                    WHERE name = ?";

        $data = $this->dbh->select($query,[$categoryName]);

        return [
            "data" => !empty($data)
        ];
    }

    //inserisce una nuova categoria
    public function insertCategory($category)
    {
        $res = $this->insert($category);
        return $res ? $res : false;
    }

    //esegue l'ìupdate di una categoria
    public function updateCategory($categoryId, $updateData)
    {
        $res = $this->update($categoryId, $updateData);
        return $res ? $res : false;
    }
}
