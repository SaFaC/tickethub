<?php
//TUTTE LE RICHIESTE VENGONO INDIRIZZATE IN QUESTO FILE

//Cartelle del sito
define("ROOT", __DIR__); //E' la root del sito
define("EVENT_IMAGE", "/public/images/events_image/"); //E' la cartella delle immagini degli eventi
define("ICON_SITE", "/public/images/icon/"); //E' la cartella della icona del sito
define("QR_TICKETS", "/public/images/qr-tickets/"); //E' la cartella dei qr-code per i biglietti 

//define per debug
define("DEBUG_ON", false); //attiva o disattiva le opzioni di debug
define("DEBUG_USER", "GST"); //E' l'utente per debug

//Informazioni del sito
define("TITLE_SITE", "Ticket Hub"); //E' la cartella della icona del sito
define("HOST", "localhost"); // E' il server a cui ti vuoi connettere.
define("USER", "sec_adm"); // E' l'utente con cui ti collegherai al DB.
define("PASSWORD", "2019TecWeb2020"); // Password di accesso al DB.
define("DATABASE", "tickethub"); // Nome del database.

//Caricamento dei file di base
require_once ROOT . "/../core/BaseModel.php";
require_once ROOT . "/../core/BaseController.php";
require_once ROOT . "/utils/functions.php";
require_once ROOT . "/utils/DatabaseHelper.php";

$app = new Application();
$app->run();

class Application
{
    //variabile che contiene l'url della richiesta
    private $url;

    // variabile che gestisce la connessione con il database
    public $dbh;

    //array associativo che contiene tutte le informazioni dell'utente che
    //visita la pagina; se è un ospite, viene salvato solo il ruolo di "GST"
    public $globalUser;

    function __construct()
    {

        $this->url = explode("/", $_SERVER["REQUEST_URI"]);

        //crea la connessione con il DB
        $this->dbh = new DatabaseHelper(HOST, USER, PASSWORD, DATABASE);

        //avviare una sessione php sicura
        sec_session_start();

        //controlla se l'utente è inserito nel cookie di sessione
        //e se è valido, estrapola l'utente
        $this->globalUser = login_check($this);


    }

    function run()
    {
        /*esempio
        /events/update/123
        
        in questo caso va a recuperare events.php nella cartella controllers
        richiamerà il metodo GET_update dell'evento con parametro l'id 123 
        */

        //$resource= events
        $resource = $this->url[1] ?: "home"; //nel primo elemento c'E' sempre qualcosa, minimo stringa vuota (che E' uguale a false)

        //$action = update
        $action = ($this->url[2] ?? "") ?: "home"; //dal secondo in poi potrebbe esserci null, quindi prima verifica il null poi verifica il valore vuoto

        //$params = 123
        $params = array_slice($this->url, 3); //estrae i parametri della richiesta, in questo caso di utilizzo dal 3 valore il poi

        //verifica se la risorsa richiesta esiste
        if (!file_exists(ROOT . "/controllers/$resource.php")) {
            $resource = "home";
            //TODO: pensare a fare un eventuale 404:
            //file nei controllers none.php
            //$resource = "none";
            //$action = "fileNotFound"
        }

        //costruzione del metodo da richiamnare(es GET_home)
        $method = "{$_SERVER['REQUEST_METHOD']}_$action";
        //carica la risorsa/controller richiesta
        require_once ROOT . "/controllers/$resource.php";
        //crea il controller per la specifica risorsa(new Controller E' riferita alla risorsa caricata sopra)
        $controllerResource = new Controller($this);

        //se viene richiesta un azione che non esiste per quella risorsa, si viene reinderizzati home del sito
        if (!method_exists($controllerResource, $method)) {
            $method = "redirect";
            $params = ["/"]; //home.php; TODO: pensare a fare un eventuale 404
            //$params = none/fileNotFound;
        }

        //esegue l'azione richiesta
        $controllerResource->$method(...$params);
    }
}
