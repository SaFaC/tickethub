<!DOCTYPE html>
<html lang="it">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no" />
    <title><?= TITLE_SITE ?></title>

    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/5.4.55/css/materialdesignicons.min.css" />

    <link rel="stylesheet" type="text/css" href="/public/styles/bootstrap.min.css" />

    <link rel="stylesheet" type="text/css" href="/public/styles/style-base.css" />
    <link rel="stylesheet" type="text/css" href="/public/styles/style-eventcard.css" />
    <link rel="stylesheet" type="text/css" href="/public/styles/style-organizations.css" />

    <!-- Icona creata da Dimi Kazak: https://www.flaticon.com/authors/dimi-kazak -->
    <link rel="shortcut icon" href="<?= ICON_SITE . "favicon.png" ?>" type="image/x-icon" />
</head>

<body>
    <!-- navbar e hamburger button-->
    <?php
    $arrayNavbar = [
        "nav_role" => $page_role ?? "GST"
    ] + (isset($activePage) ?
        ["activePage" => $activePage]
        : []);
    $this->render("Navbar", $arrayNavbar, "component");
    ?>

    <!-- SearchBar e ASIDE con carrello e login-->
    <div class="container py-3">
        <div class="row">
            <!-- SearchBar -->
            <div class="col">
                <?php $this->render("SearchBar", ["srb_text" => "Cerca per titolo..."], "component");  ?>
            </div>

            <!-- Carello e Account -->
            <aside class="col-auto ml-auto">
                <!-- Menu Carello-->
                <?php
                if ($page_role !== "GST") {
                    $this->render("ShoppingCart", [], "component");
                }
                ?>

                <!-- Menu Login -->
                <?php
                if ($page_role === "GST") {
                    $this->render("LoginMenu", [], "component");
                }
                ?>

                <!-- Menu Account -->
                <?php
                if ($page_role !== "GST") {
                    $this->render("AccountMenu", [], "component");
                }
                ?>

            </aside>
        </div>
    </div>

    <!-- MAIN -->
    <main class="container min-vh-100">

        <?= $main ?>

    </main>


    <!-- FOOTER -->
    <?php $this->render("Footer", [], "component"); ?>



    <!-- Librerie -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script src="/public/scripts/html5-qrcode.min.js"></script>
    <script src="/public/scripts/sha512.js"></script>
    <script src="/public/scripts/cookie.js"></script>

    <!-- Script -->
    <script src="/public/scripts/cookie-banner.js"></script>
    <script src="/public/scripts/frmLogin.js"></script>
    <script src="/public/scripts/shopping-cart.js"></script>
    <script src="/public/scripts/qr-scanning.js"></script>
    <script src="/public/scripts/toasty.js"></script>
</body>

</html>