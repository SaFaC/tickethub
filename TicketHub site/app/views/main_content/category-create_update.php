<?php /* Il tag main della registrazione  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
    $error, contiene un eventuale messaggio di erroreda mostrare tramite alert
    $success, contiene un eventuale messaggio di successo da mostrare tramite alert
    $value, array associativo che contiene i valori che devono avere gli input (utlizzato in caso di edit)
        category_id
        name
        description
        disabled, visualizza la checkbox solo se è l'admin a richiederla
 */ ?>
<?php $create = isset($value) ? false : true; ?>

<header class="py-3">
    <h1 class="mb-5 text-break"><?= $create ? "Creazione" : "Modifica" ?> Categoria</h1>
</header>

<form 
    method="POST"
    action="/categories/<?= $create ? "create" : "update/{$value["category_id"]}" ?>"
>

    <?php
    //messaggio di errore
    if (isset($error)) {
        $this->render(
            "Alert",
            [
                "alert" => [
                    "info" => "alert-danger",
                    "text" => "$error"
                ]
            ],
            "component"
        );
        //messaggio di successo
    } else if (isset($success)) {
        $this->render(
            "Alert",
            [
                "alert" => [
                    "info" => "alert-success",
                    "text" => "$success"
                ]
            ],
            "component"
        );
    }

    //input per il nome
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "text",
                "idElm" => "input-category-name",
                "name" => "category-name",
                "lblText" => "Nome",
                "required" => true,
                "value" => $create ? "" : $value["name"]
            ],
        ],
        "component"
    );

    //input per la descrizione
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "textarea",
                "idElm" => "textarea-category-description",
                "name" => "category-description",
                "lblText" => "Descrizione",
                "required" => true,
                "value" => $create ? "" : $value["description"]
            ],
        ],
        "component"
    );

    if ($page_role === "ADM") {
        //check per disabilitare l'categoria
        $this->render(
            "FormGroup",
            [
                "elmForm" => [
                    "type" => "checkbox",
                    "idElm" => "input-category-disabled",
                    "name" => "category-disabled",
                    "lblText" => "Disabilita categoria",
                    "required" => false,
                    "value" => $create ? "on" : $value["disabled"]
                ],
            ],
            "component"
        );
    }
    ?>

    <div class="text-right">
        <button type="submit" class="btn btn-success"><?= $create ? "Crea" : "Salva" ?></button>
    </div>
</form>