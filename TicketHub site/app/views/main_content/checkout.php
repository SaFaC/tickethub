<?php /* Il tag main della Home  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
 
 */ ?>
<div class="row justify-content-center">
    <div class="col p-3">
        <div class="card">
            <header>
                <h1 class="card-header h4 text-center">Riepilogo del carrello</h1>
            </header>

            <div id="cart-list" class="card-body">
                <ul class="list-unstyled mb-0">

                </ul>
            </div>

            <div id="total-price"></div>

            <footer id="cart-list-footer" class="d-flex card-footer bg-transparent justify-content-between py-3">
                <button class="btn btn-danger">
                    <span class="d-none d-sm-inline">Svuota carrello</span>
                    <span class="sr-only d-sm-none">Svuota carrello</span>
                    <span class="mdi mdi-cart-remove"></span>
                </button>
                <button class="btn btn-success">
                    <span class="d-none d-sm-inline">Acquista</span>
                    <span class="sr-only d-sm-none">Acquista</span>
                    <span class="mdi mdi-cart-arrow-right"></span>
                </button>
            </footer>

        </div>
    </div>
</div>