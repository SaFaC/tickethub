<?php /* Il tag main della Home  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:
    $users, array associativo con tutti gli utenti e i rispettivi dati
 */ ?>

<header class="py-3">
    <h1 class="mb-5">Gestione Utenti</h1>
</header>

<div class="table-responsive">
    <table class="table table-bordered">
        <caption class="sr-only">Elenco degli utenti presenti del database di TicketHub.</caption>

        <thead>
            <tr class="bg-light">
                <th class="align-middle text-center" scope="col" id="col-id">ID</th>
                <th class="align-middle" scope="col" id="col-name">Nome</th>
                <th class="align-middle" scope="col" id="col-surname">Cognome</th>
                <th class="align-middle" scope="col" id="col-email">Email</th>
                <th class="align-middle" scope="col" id="col-role">Ruolo</th>
                <th class="align-middle text-center" scope="col" id="col-enabled">Attivo</th>
                <th class="align-middle text-center" scope="col" id="col-action">Azioni</th>
            </tr>
        </thead>

        <tbody>

            <?php foreach ($users as $user) : ?>
                <tr>
                    <td class="align-middle text-center" headers="col-id"><?= $user["user_id"] ?></td>
                    <td class="align-middle" headers="col-name"><?= $user["name"] ?></td>
                    <td class="align-middle" headers="col-surname"><?= $user["surname"] ?></td>
                    <td class="align-middle" headers="col-email"><?= $user["email"] ?></td>
                    <td class="align-middle" headers="col-role"><?= $user["role"] ?></td>
                    <td class="align-middle text-center" headers="col-enabled">
                        <?php if ($user["disabled"]) : ?>
                            <span class="mdi mdi-close text-danger h3">
                                <span class="d-none">Disattiva</span>
                            </span>
                        <?php else : ?>
                            <span class="mdi mdi-check text-success h3">
                                <span class="d-none">Attivato</span>
                            </span>
                        <?php endif; ?>
                    </td>
                    <td class="align-middle text-center text-nowrap" headers="col-action">
                        <a href="/users/update/<?= $user["user_id"] ?>" class="btn btn-primary btn-sm">
                            <span class="d-none d-lg-inline">Modifica</span>
                            <span class="sr-only d-lg-none">Modifica</span>
                            <span class="mdi mdi-pencil"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
</div>