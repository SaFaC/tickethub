<?php /* Il tag main della Home  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
    $singularEvent, l'array associativo con i dettagli dell'evento da visualizzare:
        image, indica il nome dell'immagine dell'evento da visualizzare
        title, è il titolo dell'evento
        category, è la categoria dell'evento
        description, è la descrizione estesa dell'evento
        starts_at, è la data di inziio
        seats, sono i posti disponibili
        riority_seats, sono i posti per disabili disponibili
        duration, la durata in minuti dell'evento
        address, l'indirizzo dove si trova l'evento
        price, è il prezzo del biglietto dell'evento
        event_id, è l'id dell'evento da utilizzare per il visualizza dettagli
    $page_role, indica il ruolo di chi sta visualizzando la pagina:
        GST = ospite
        USR = utente registrato (loggato)
        ORG = utente organizzatore (loggato)
        ADM = utente admin del sistema (loggato)
    $canEdit, indica se far comparire il bottone di modifica o no
 */ ?>
<header class="sr-only">
    <h1>Dettaglio evento</h1>
</header>

<div class="row">
    <?php
    //carica delle card evento
    $this->render("CardDetails", [
        "singularEvent" => $singularEvent,
        "page_role" => $page_role,
        "canEdit" => $canEdit
    ], "component");
    ?>
</div>