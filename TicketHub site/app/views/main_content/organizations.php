<?php /* Il tag main della Home  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:
    $organizations, array associativo con tutte le organizzazioni e i rispettivi dati
 */ ?>

<header class="py-3">
    <h1 class="mb-5">Gestione Organizzazioni</h1>
</header>

<div class="table-responsive">
    <table class="table table-bordered">
        <caption class="sr-only">Elenco delle organizzazioni presenti del database di TicketHub.</caption>

        <thead>
            <tr class="bg-light">
                <th class="align-middle text-center" scope="col" id="col-id">ID</th>
                <th class="align-middle" scope="col" id="col-name">Nome</th>
                <th class="align-middle" scope="col" id="col-owner">Proprietario</th>
                <th class="align-middle" scope="col" id="col-description">Descrizione</th>
                <th class="align-middle" scope="col" id="col-address">Indirizzo</th>
                <th class="align-middle text-center" scope="col" id="col-enabled">Attiva</th>
                <th class="align-middle text-center" scope="col" id="col-action">Azioni</th>
            </tr>
        </thead>

        <tbody>

            <?php foreach ($organizations as $organization) : ?>
                <tr>
                    <td class="align-middle text-center" headers="col-id"><?= $organization["organization_id"] ?></td>
                    <td class="align-middle" headers="col-name"><?= $organization["name"] ?></td>
                    <td class="align-middle" headers="col-owner"><?= $organization["founder_name"] ?></td>
                    <td class="align-middle" headers="col-description"><?= $organization["description"] ?></td>
                    <td class="align-middle" headers="col-address"><?= $organization["address"] ?></td>
                    <td class="align-middle text-center" headers="col-enabled">
                        <?php if ($organization["disabled"]) : ?>
                            <span class="mdi mdi-close text-danger h3">
                                <span class="d-none">Disattiva</span>
                            </span>
                        <?php else : ?>
                            <span class="mdi mdi-check text-success h3">
                                <span class="d-none">Attiva</span>
                            </span>
                        <?php endif; ?>
                    </td>
                    <td class="align-middle text-center" headers="col-action">
                        <a href="/organizations/update/<?= $organization["organization_id"] ?>" class="btn btn-primary btn-sm">
                            <span class="d-none d-lg-inline">Modifica</span>
                            <span class="sr-only d-lg-none">Modifica</span>
                            <span class="mdi mdi-pencil"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
</div>