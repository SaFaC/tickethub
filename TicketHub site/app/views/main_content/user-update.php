<?php /* Il tag main della modifica utente  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
    $error, contiene un eventuale messaggio di erroreda mostrare tramite alert
    $success, contiene un eventuale messaggio di successo da mostrare tramite alert
    $value, array associativo che contiene i valori che devono avere gli input
        user_id
        name
        surname
        email
        role
        organization
        disabled, visualizza la checkbox solo se è l'admin a richiederla
    roles, array di array associativo (elenco dei ruoli):
        opt_val, il valore da passare al server
        selected, indica se è selezionato
        opt_text, il testo associato al valore
    organizations, array di array associativo (elenco delle organizzazioni):
        opt_val, il valore da passare al server
        selected, indica se è selezionato
        opt_text, il testo associato al valore
    isOwner, indica se si sta accedendo al proprio profilo
 */ ?>

<header class="py-3">
    <h1 class="mb-5 text-break"><?= $isOwner ? "Gestione profilo" : "Modifica Utente" ?></h1>
</header>

<form class="crypt" method="POST" action="/users/update/<?= $value["user_id"] ?>">

    <?php
    //messaggio di errore
    if (isset($error)) {
        $this->render(
            "Alert",
            [
                "alert" => [
                    "info" => "alert-danger",
                    "text" => "$error"
                ]
            ],
            "component"
        );
        //messaggio di successo
    } else if (isset($success)) {
        $this->render(
            "Alert",
            [
                "alert" => [
                    "info" => "alert-success",
                    "text" => "$success"
                ]
            ],
            "component"
        );
    }

    //input per il nome
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "text",
                "idElm" => "input-user-name",
                "name" => "user-name",
                "lblText" => "Nome",
                "required" => true,
                "value" => $value["name"]
            ],
        ],
        "component"
    );

    //input per il cognome
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "text",
                "idElm" => "input-user-surname",
                "name" => "user-surname",
                "lblText" => "Cognome",
                "required" => true,
                "value" => $value["surname"]
            ],
        ],
        "component"
    );

    if ($isOwner) {
        //input per la email
        $this->render(
            "FormGroup",
            [
                "elmForm" => [
                    "type" => "email",
                    "idElm" => "input-user-email",
                    "name" => "user-email",
                    "lblText" => "Indirizzo Email",
                    "required" => true,
                    "value" => $value["email"]
                ],
            ],
            "component"
        );

        //cambia password
        $this->render(
            "FormGroup",
            [
                "elmForm" => [
                    "type" => "password",
                    "idElm" => "input-user-password",
                    "name" => "pd",
                    "lblText" => "Nuova password (solo se vuoi cambarla)",
                    "class" => "password",
                    "required" => false,
                ],
            ],
            "component"
        );

        //conferma cambio password
        $this->render(
            "FormGroup",
            ["elmForm" => [
                "type" => "password",
                "idElm" => "input-user-password-check",
                "name" => "cp",
                "lblText" => "Conferma nuova password (solo se vuoi cambarla)",
                "class" => "password-check",
                "required" => false,
            ],],
            "component"
        );
    }


    if ($page_role === "ADM") {

        //input il ruolo
        $this->render(
            "FormGroup",
            [
                "elmForm" => [
                    "type" => "select",
                    "idElm" => "input-user-role",
                    "name" => "user-role",
                    "lblText" => "Ruolo",
                    "required" => true,
                    "value" => $value["role"],
                    "select" => $roles
                ],
            ],
            "component"
        );

        //input l'organizzazione
        $this->render(
            "FormGroup",
            [
                "elmForm" => [
                    "type" => "select",
                    "idElm" => "input-user-organization",
                    "name" => "user-organization_id",
                    "lblText" => "Organizzazione",
                    "required" => false,
                    "value" => $value["organization"],
                    "select" => $organizations
                ],
            ],
            "component"
        );

        //check per disabilitare utente
        $this->render(
            "FormGroup",
            [
                "elmForm" => [
                    "type" => "checkbox",
                    "idElm" => "input-user-disabled",
                    "name" => "user-disabled",
                    "lblText" => "Disabilita utente",
                    "required" => false,
                    "value" => $value["disabled"]
                ],
            ],
            "component"
        );
        // echo    `<div class="form-group">
        //             <a href="auth/pwReset/$value["user_id"]" class="btn btn-primary">Resetta Password</a>
        //         </div>`;
    }
    ?>


    <div class="text-right">
        <button type="submit" class="btn btn-success">Salva</button>
    </div>
</form>