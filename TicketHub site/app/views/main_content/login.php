<?php /* Il tag main della pagina di login  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
    $error, contiene il messaggio di errore in caso di fallito tentativo di login
        null se è andato a buon fine
 */ ?>

<div class="row justify-content-center">

    <div class="col-md-9 col-lg-7 col-xl-6 p-3">
        <div class="card">
            <header>
                <h1 class="card-header h4 text-center">Login</h1>
            </header>
            <div class="card-body">

                <?php if (isset($error)) { //se error è settata, allora la richiesta viene dopo una registrazione
                    if ($error) { //se error ha un valore, allora la registrazione è fallita
                        $this->render(
                            "Alert",
                            ["alert" => [
                                "info" => "alert-danger",
                                "text" => "Errore: $error"
                            ]],
                            "component"
                        );
                    }
                } ?>

                <!-- Registration Form -->
                <form class="crypt" method="POST" action="/auth/login">
                    <!-- Username (email) -->
                    <?php
                    $this->render(
                        "FormGroup",
                        [
                            "elmForm" => [
                                "type" => "email",
                                "idElm" => "page-login-email",
                                "name" => "em",
                                "lblText" => "Indirizzo email",
                                "required" => true,
                            ],
                        ],
                        "component"
                    );
                    ?>
                    <!-- Password -->
                    <?php
                    $this->render(
                        "FormGroup",
                        [
                            "elmForm" => [
                                "type" => "password",
                                "idElm" => "page-login-password",
                                "lblText" => "Password",
                                "name" => "pd",
                                "class" => "password",
                                "required" => true,
                            ],
                        ],
                        "component"
                    );
                    ?>

                    <?php
                    // <!-- Checkbox ricordami -->
                    // $this->render("FormGroup", ["elmForm" => [
                    //     "type" => "checkbox",
                    //     "idElm" => "page-login-remember",
                    //     "lblText" => "Ricordami",
                    //     "name" => "remember",
                    //     "required" => false,
                    // ],], "component");
                    ?>

                    <div class="form-group text-right">
                        <button type="submit" class="btn btn-primary">
                            Accedi
                        </button>
                    </div>
                    <div class="text-left">
                        <a href="/auth/pwLost">Password dimenticata</a>
                        |
                        <a href="/auth/register">Non hai un account? Registrati!</a>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>