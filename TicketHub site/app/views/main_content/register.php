<?php /* Il tag main della registrazione  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
    $error, contiene il messaggio di errore in caso di fallito tentativo di registrazione
        null se è andato a buon fine
 */ ?>

<div class="row justify-content-center">

    <div class="col-md-9 col-lg-7 col-xl-6 p-3">
        <div class="card">
            <header>
                <h1 class="card-header h4 text-center">Registrazione</h1>
            </header>
            <div class="card-body">

                <?php if (isset($error)) { //se error è settata, allora la richiesta viene dopo una registrazione
                    if ($error) { //se error ha un valore, allora la registrazione è fallita
                        $this->render(
                            "Alert",
                            ["alert" => [
                                "info" => "alert-danger",
                                "text" => "Errore: $error"
                            ]],
                            "component"
                        );
                    } else { //error vale false, la registrazione è andata a buon fine
                        $this->render(
                            "Alert",
                            ["alert" => [
                                "info" => "alert-success",
                                "text" => 'Account creato con successo, puoi ora
                                            <a href="/auth/login" class="alert-link">effettuare il login.</a>'
                            ]],
                            "component"
                        );
                    }
                } ?>


                <!-- Registration Form -->
                <form class="crypt" method="POST" action="/auth/register">
                    <?php
                    $this->render(
                        "FormGroup",
                        [
                            "elmForm" => [
                                "type" => "text",
                                "idElm" => "input-name",
                                "name" => "name",
                                "lblText" => "Nome",
                                "required" => true,
                            ],
                        ],
                        "component"
                    );

                    $this->render(
                        "FormGroup",
                        [
                            "elmForm" => [
                                "type" => "text",
                                "idElm" => "input-surname",
                                "name" => "surname",
                                "lblText" => "Cognome",
                                "required" => true,
                            ],
                        ],
                        "component"
                    );

                    $this->render(
                        "FormGroup",
                        [
                            "elmForm" => [
                                "type" => "email",
                                "idElm" => "input-email",
                                "name" => "em",
                                "lblText" => "Indirizzo email",
                                "required" => true,
                            ],
                        ],
                        "component"
                    );

                    $this->render(
                        "FormGroup",
                        [
                            "elmForm" => [
                                "type" => "password",
                                "idElm" => "input-password",
                                "name" => "pd",
                                "lblText" => "Password",
                                "class" => "password",
                                "required" => true,
                            ],
                        ],
                        "component"
                    );

                    $this->render(
                        "FormGroup",
                        ["elmForm" => [
                            "type" => "password",
                            "idElm" => "input-password-check",
                            "name" => "cp",
                            "lblText" => "Conferma password",
                            "class"=>"password-check",
                            "required" => true,
                        ],],
                        "component"
                    );
                    ?>

                    <div class="row align-items-center">
                        <div class="col mb-0">
                            <a class="mb-0" href="/auth/login">Sei già registrato?</a>
                        </div>
                        <div class="col-auto ml-auto">
                            <button type="submit" class="btn btn-primary">
                                Registrati
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>

</div>