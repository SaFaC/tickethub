<?php /* Il tag main della Home  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:
    $events, array associativo con tutti gli eventi e i rispettivi dati
 */ ?>

<header class="py-3">
    <h1 class="mb-5">Gestione Eventi</h1>
</header>

<div class="table-responsive">
    <table class="table table-bordered">
        <caption class="sr-only">Elenco degli eventi presenti del database di TicketHub.</caption>

        <thead>
            <tr class="bg-light">
                <th class="align-middle text-center" scope="col" id="col-id">ID</th>
                <th class="align-middle" scope="col" id="col-name">Nome</th>
                <th class="align-middle" scope="col" id="col-organization">Organizzazione</th>
                <th class="align-middle" scope="col" id="col-date">Data</th>
                <th class="align-middle text-center" scope="col" id="col-enabled">Attivo</th>
                <th class="align-middle text-center" scope="col" id="col-action">Azioni</th>
            </tr>
        </thead>

        <tbody>

            <?php foreach ($events as $event) : ?>
                <tr>
                    <td class="align-middle text-center" headers="col-id"><?= $event["event_id"] ?></td>
                    <td class="align-middle" headers="col-name"><?= $event["name"] ?></td>
                    <td class="align-middle" headers="col-organization"><?= $event["organization_name"] ?></td>
                    <td class="align-middle" headers="col-date">
                        <div class="text-nowrap">
                            <?= $event["starts_at"] ?>
                        </div>
                        <div class="text-nowrap">
                            <?= $event["ends_at"] ?>
                        </div>
                    </td>
                    <td class="align-middle text-center" headers="col-enabled">
                        <?php if ($event["disabled"]) : ?>
                            <span class="mdi mdi-close text-danger h3">
                                <span class="d-none">Disattiva</span>
                            </span>
                        <?php else : ?>
                            <span class="mdi mdi-check text-success h3">
                                <span class="d-none">Attivo</span>
                            </span>
                        <?php endif; ?>
                    </td>
                    <td class="align-middle text-center text-nowrap" headers="col-action">
                        <a href="/events/details/<?= $event["event_id"] ?>" class="btn btn-primary btn-sm">
                            <span class="d-none d-lg-inline">Visualizza</span>
                            <span class="sr-only d-lg-none">Visualizza</span>
                            <span class="mdi mdi-eye"></span>
                        </a>
                        <a href="/events/update/<?= $event["event_id"] ?>" class="btn btn-primary btn-sm">
                            <span class="d-none d-lg-inline">Modifica</span>
                            <span class="sr-only d-lg-none">Modifica</span>
                            <span class="mdi mdi-pencil"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
</div>