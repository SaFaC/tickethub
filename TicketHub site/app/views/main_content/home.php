<?php /* Il tag main della Home  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
    $eventsHome, l'array associativo con tutti gli eventi da visualizzare nella home:
        image, contiene il nome del file (compresa estensione) dell'immagine
        name, è il titolo
        description, contiene la descrizione (gia troncata)
        price, è il prezzo di vendita del biglietto
        event_id, è l'id
    $pagination, l'array associativo che contiene le informazioni per la paginazione:
        firstPage, è il numero della prima pagina da mostrare
        pageSelected, è il numero di scheda selezionato (da 1 a 3)
        pageDisabled, è un array booleano di 5 valori, indica quali pulsanti disabilitare
        lastPage, indica l'ultima pagina disponibile
    qualcosa per quanto riguarda la paginazione
 */ ?>
<header class="sr-only">
    <h1>Home</h1>
</header>

<div class="row">
    <?php foreach ($eventsHome as $singularEvent) {
        //esegue le istruzioni per ogni evento che viene passato

        //carica delle card evento
        $this->render("CardHome", [
            "singularEvent" => $singularEvent,
        ], "component");
    } ?>
</div>

<?php $this->render("PaginationNav", ["pagination" => $pagination], "component"); ?>