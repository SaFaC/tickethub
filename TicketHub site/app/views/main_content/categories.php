<?php /* Il tag main della Home  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:
    $categories, array associativo con tutte le categorie e i rispettivi dati
 */ ?>

<header class="py-3">
    <h1>Gestione Categorie</h1>
    <div class="text-right">
        <a href="/categories/create" class="btn btn-success">
            Nuova categoria
            <span class="mdi mdi-tag-plus-outline"></span>
        </a>
    </div>
</header>


<div class="table-responsive">
    <table class="table table-bordered">
        <caption class="sr-only">Elenco delle categorie presenti del database di TicketHub.</caption>

        <thead>
            <tr class="bg-light">
                <th class="align-middle text-center" scope="col" id="col-id">ID</th>
                <th class="align-middle w-25" scope="col" id="col-name">Nome</th>
                <th class="align-middle w-50" scope="col" id="col-description">Descrizione</th>
                <th class="align-middle text-center" scope="col" id="col-enabled">Attiva</th>
                <th class="align-middle text-center" scope="col" id="col-action">Azioni</th>
            </tr>
        </thead>

        <tbody>
            <?php foreach ($categories as $category) : ?>
                <tr>
                    <td class="align-middle text-center" headers="col-id"><?= $category["category_id"] ?></td>
                    <td class="align-middle" headers="col-name"><?= $category["name"] ?></td>
                    <td class="align-middle" headers="col-description"><?= $category["description"] ?></td>
                    <td class="align-middle text-center" headers="col-enabled">
                        <?php if ($category["disabled"]) : ?>
                            <span class="mdi mdi-close text-danger h3">
                                <span class="d-none">Disattiva</span>
                            </span>
                        <?php else : ?>
                            <span class="mdi mdi-check text-success h3">
                                <span class="d-none">Attiva</span>
                            </span>
                        <?php endif; ?>
                    </td>
                    <td class="align-middle text-center" headers="col-action">
                        <a href="/categories/update/<?= $category["category_id"] ?>" class="btn btn-primary btn-sm">
                            <span class="d-none d-lg-inline">Modifica</span>
                            <span class="mdi mdi-pencil"></span>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>

        </tbody>
    </table>
</div>