<?php /* Il tag main della registrazione  */ ?>
<?php /* Variabili PHP che necessita questa pagina per funzionare:  
    $error, contiene un eventuale messaggio di erroreda mostrare tramite alert
    $success, contiene un eventuale messaggio di successo da mostrare tramite alert
    $value, array associativo che contiene i valori che devono avere gli input (utlizzato in caso di edit)
        name
        image
        description
        address
        seats
        priority-seats
        price
        date-start
        date-end
        disabled, visualizza la checkbox solo se è l'admin a richiederla
 */ ?>
<?php $create = isset($value) ? false : true; ?>

<header class="py-3">
    <h1 class="mb-5 text-break"><?= $create ? "Creazione" : "Modifica" ?> Evento</h1>
</header>
<form 
    enctype="multipart/form-data"
    method="POST"
    action="/events/<?= $create ? "create" : "update/{$value["event_id"]}" ?>"
>

    <?php
    //messaggio di errore
    if (isset($error)) {
        $this->render(
            "Alert",
            [
                "alert" => [
                    "info" => "alert-danger",
                    "text" => "$error"
                ]
            ],
            "component"
        );
        //messaggio di successo
    } else if (isset($success)) {
        $this->render(
            "Alert",
            [
                "alert" => [
                    "info" => "alert-success",
                    "text" => "$success"
                ]
            ],
            "component"
        );
    }

    //input per il nome
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "text",
                "idElm" => "input-event-name",
                "name" => "event-title",
                "lblText" => "Nome",
                "required" => true,
                "value" => $create ? "" : $value["name"]
            ],
        ],
        "component"
    );

    //input per l'immagine
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "file",
                "idElm" => "input-event-image",
                "name" => "event-file",
                "lblText" => "Immagine (sono consigliate immagini quadrate)",
                "required" => $create ? true : false,
                "value" => $create ? "" : $value["image"]
            ],
        ],
        "component"
    );

    //input la categoria
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "select",
                "idElm" => "input-event-category",
                "name" => "event-category",
                "lblText" => "Categoria",
                "required" => true,
                "value" => $create ? "" : $value["category"],
                "select" => $categories
            ],
        ],
        "component"
    );

    //input per la descrizione
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "textarea",
                "idElm" => "textarea-event-description",
                "name" => "event-description",
                "lblText" => "Descrizione",
                "required" => true,
                "value" => $create ? "" : $value["description"]
            ],
        ],
        "component"
    );

    //input per l'indirizzo
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "text",
                "idElm" => "input-event-address",
                "name" => "event-address",
                "lblText" => "Indirizzo",
                "required" => true,
                "value" => $create ? "" : $value["address"]
            ],
        ],
        "component"
    );

    //input per i posti disponibili
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "number",
                "idElm" => "input-event-seats",
                "name" => "event-seats",
                "lblText" => "Posti disponibili",
                "required" => true,
                "value" => $create ? "" : $value["seats"],
                "min" => 0,
                "max" => 5000
            ],
        ],
        "component"
    );

    //input per i posti per disabili disponibili
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "number",
                "idElm" => "input-event-seats-priority",
                "name" => "event-priority-seats",
                "lblText" => "Posti prioritari disponibili",
                "required" => true,
                "value" => $create ? "" : $value["priority_seats"],
                "min" => 0,
                "max" => 5000
            ],
        ],
        "component"
    );

    //input per il prezzo
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "number",
                "idElm" => "input-event-price",
                "name" => "event-price",
                "lblText" => "Prezzo",
                "required" => true,
                "value" => $create ? "" : $value["price"],
                "min" => "0.00",
                "step" => "0.01"
            ],
        ],
        "component"
    );

    //input per la data di inzio evento
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "datetime-local",
                "idElm" => "input-event-startdate",
                "name" => "event-startdate",
                "lblText" => "Data inizio",
                "required" => true,
                "value" => $create ? "" : $value["starts_at"]
            ],
        ],
        "component"
    );


    //input per la data di fine evento
    $this->render(
        "FormGroup",
        [
            "elmForm" => [
                "type" => "datetime-local",
                "idElm" => "input-event-enddate",
                "name" => "event-enddate",
                "lblText" => "Data fine",
                "required" => true,
                "value" => $create ? "" : $value["ends_at"]
            ],
        ],
        "component"
    );

    if ($page_role === "ADM") {
        //check per disabilitare l'evento
        $this->render(
            "FormGroup",
            [
                "elmForm" => [
                    "type" => "checkbox",
                    "idElm" => "input-event-disabled",
                    "name" => "event-disabled",
                    "lblText" => "Disabilita evento",
                    "required" => false,
                    "value" => $create ? "" : $value["disabled"]
                ],
            ],
            "component"
        );
    }
    ?>

    <div class="text-right">
        <button type="submit" class="btn btn-success"><?= $create ? "Crea" : "Salva" ?></button>
    </div>
</form>