<?php /* Alert dimostrativo senza animazione */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:
    $alert, array associativo che contiene le info dell'alert:
        info, indica il tipo di informazione dell'alert(es. array-danger, alert-success)
        text, è il testo dell'alert
 */ ?>
<div class="alert <?= $alert["info"] ?>">
    <?= $alert["text"] ?>
</div>