<?php /* Menu' Login */ ?>
<div class="btn-group dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
        <span class="d-none d-md-inline">Accedi</span>
        <span class="sr-only d-md-none">Accedi</span>
        <span class="mdi mdi-account-circle"></span>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <form class="px-4 py-3 crypt" method="POST" action="/auth/login">
            <!-- Username (email) -->
            <?php
            $this->render("FormGroup", ["elmForm" => [
                "type" => "email",
                "idElm" => "login-email",
                "name" => "em",
                "lblText" => "Indirizzo email",
                "required" => true,
            ],], "component");
            ?>
            <!-- Password -->
            <?php
            $this->render("FormGroup", ["elmForm" => [
                "type" => "password",
                "idElm" => "login-password",
                "lblText" => "Password",
                "name" => "pd",
                "class" =>"password",
                "required" => true,
            ],], "component");
            ?>
            <?php
            // <!-- Checkbox ricordami -->
            // $this->render("FormGroup", ["elmForm" => [
            //     "type" => "checkbox",
            //     "idElm" => "login-remember",
            //     "lblText" => "Ricordami",
            //     "name" => "remember",
            //     "required" => false,
            // ],], "component");
            ?>
            <!-- Submit button -->
            <button type="submit" class="btn btn-primary btn-block">Accedi</button>
        </form>
        <div class="dropdown-divider"></div>
        <!-- Register -->
        <a class="dropdown-item" href="/auth/register">Non hai un account? Registrati!</a>
        <div class="dropdown-divider"></div>
        <!-- TODO: Password lost -->
        <a class="dropdown-item" href="/auth/pwLost">Password dimenticata?</a>
    </div>
</div>