<?php /* Barra di ricerca */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:  
    $srb_text, il testo da far visualizzare nel placeholder del cerca
 */ ?>
<form id="search" action="/" method="GET">
    <label class="sr-only" for="search-bar">Cerca eventi</label>
    <div class="input-group">
        <input id="search-bar" type="text" name="search" class="form-control" placeholder="<?= $srb_text ?>">
        <div class="input-group-append">
            <button class="btn btn-outline-secondary" type="submit">
                <span class="mdi mdi-magnify"></span>
                <span class="sr-only">Avvia ricerca</span>
            </button>
        </div>
    </div>
</form>