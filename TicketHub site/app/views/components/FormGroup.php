<?php /* Elemento label+input dei form */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:
    $elmForm, array associativo che contiene le informazioni dell'elemento form:
        type, è il tipo del componente input da visualizzare
            text
            file
            textarea
            number
            datetime-local
            email
            password
            checkbox
        idElm, è l'id da associare alla label e all'input; il name dell'input sarà type_idElm
        name, è il nome da dare alla variabile che gestirà il valore dell'input
        lblText, è il testo della label
        required, booleano che indica se l'input è richiesto
        min, per i type number, indica il valore minimo
        max, per i type number, indica il valore massimo
        step, per i type number, indica lo step da fare
        class, per aggiungere eventuali classi
        value, per visualizzare un valore di default
        select, array di array associativo contenente la coppia valore-booleano per il menu a tendina:
            opt_val, il valore da passare al server
            selected, indica se è selezionato
            opt_text, il testo associato al valore
 */ ?>
<?php 
    //setto a false i valori opzionali se sono null
    $isMin = isset($elmForm["min"]) ? true : false;
    $isMax = isset($elmForm["max"]) ? true : false;
    $isStep = isset($elmForm["step"]) ? true : false;
    $elmForm["class"] = $elmForm["class"] ?? "";
    $isVal = isset($elmForm["value"]) ? true : false;
?>
<?php if ($elmForm["type"] !== "checkbox") : ?>
    <div class="form-group">
        <label for="<?= $elmForm["idElm"] ?>">
            <?= $elmForm["lblText"] ?>
        </label>

        <?php if ($elmForm["type"] === "text") : 
        //TEXT ?>
            <input 
                type="text" 
                class="form-control <?= $elmForm["class"] ?>" 
                id="<?= $elmForm["idElm"] ?>" 
                name="<?= $elmForm["name"] ?>" 
                <?= $elmForm["required"] ? "required" : "" ?> 
                <?= $isVal ? "value='{$elmForm["value"]}'" : "" ?> 
            />

        <?php elseif ($elmForm["type"] === "file") : 
        //FILE ?>
        <?php if($elmForm["value"]): ?>
            <label for="<?= $elmForm["idElm"] ?>">
                - <?= $elmForm["value"] ?>
            </label>
        <?php endif; ?>
            <input 
                type="file" 
                class="form-control-file <?= $elmForm["class"] ?>" 
                id="<?= $elmForm["idElm"] ?>" 
                name="<?= $elmForm["name"] ?>"
                <?= $elmForm["required"] ? "required" : "" ?> 
                accept=".png, .jpg, .jpeg, .bmp" 
            />

        <?php elseif ($elmForm["type"] === "textarea") : 
        //TEXTAREA ?>
            <textarea 
                class="form-control <?= $elmForm["class"] ?>"
                id="<?= $elmForm["idElm"] ?>"
                name="<?= $elmForm["name"] ?>"
                <?= $elmForm["required"] ? "required" : "" ?>
                rows="3" 
            ><?= $isVal ? $elmForm["value"] : "" ?></textarea>

        <?php elseif ($elmForm["type"] === "number") :
        //NUMBER ?>
            <input 
                type="number" 
                class="form-control <?= $elmForm["class"] ?>" 
                id="<?= $elmForm["idElm"] ?>" 
                name="<?= $elmForm["name"] ?>" 
                <?= $elmForm["required"] ? "required" : "" ?> 
                <?= $isVal ? "value='{$elmForm["value"]}'" : "" ?> 
                <?= $isMin? "min={$elmForm["min"]}" : "" ?>
                <?= $isMax ? "max={$elmForm["max"]}" : "" ?>
                <?= $isStep ? "step={$elmForm["step"]}" : "" ?> 
            />

        <?php elseif ($elmForm["type"] === "datetime-local") :
        //DATETIME-LOCAL ?>
            <input 
                type="datetime-local" 
                class="form-control <?= $elmForm["class"] ?>" 
                id="<?= $elmForm["idElm"] ?>" 
                name="<?= $elmForm["name"] ?>" 
                <?= $elmForm["required"] ? "required" : "" ?> 
                <?= $isVal ? "value='{$elmForm["value"]}'" : "" ?>  
            />

        <?php elseif ($elmForm["type"] === "email") :
        //EMAIL ?>
            <input 
                type="email" 
                class="form-control <?= $elmForm["class"] ?>" 
                id="<?= $elmForm["idElm"] ?>"
                name="<?= $elmForm["name"] ?>" 
                <?= $elmForm["required"] ? "required" : "" ?> 
                <?= $isVal ? "value='{$elmForm["value"]}'" : "" ?>  
                placeholder="email@example.com" 
            />

        <?php elseif ($elmForm["type"] === "password") :
        //PASSWORD ?>
            <input
                type="password"
                class="form-control <?= $elmForm["class"] ?>"
                id="<?= $elmForm["idElm"] ?>"
                name="<?= $elmForm["name"] ?>"
                <?= $elmForm["required"] ? "required" : "" ?>
                <?= $isVal ? "value='{$elmForm["value"]}'" : "" ?>
                placeholder="Password"
            />

        <?php elseif ($elmForm["type"] === "select") :
        //SELECT 
        ?>
            <select 
                class="form-control" 
                id="<?= $elmForm["idElm"] ?>"
                name="<?= $elmForm["name"] ?>"
                <?= $elmForm["required"] ? "required" : "" ?>
            >
            <option></option>
            <?php foreach ($elmForm["select"] as $option): ?>
            <option
                value="<?= $option["opt_val"] ?>"
                <?= $option["selected"] ? "selected" : "" ?>
            ><?= $option["opt_text"] ?></option>
            <?php endforeach; ?>
            </select> 
        <?php endif; ?>
    <?php else : 
    //CHECKBOX ?>
        <div class="form-group form-check">
            <input
                type="checkbox"
                class="form-check-input <?= $elmForm["class"] ?>"
                id="<?= $elmForm["idElm"] ?>"
                name="<?= $elmForm["name"] ?>"
                <?= $elmForm["required"] ? "required" : "" ?>
                <?= $isVal ? ($elmForm["value"] ? "checked" : "") :"" ?>
            />
            <label class="form-check-label" for="<?= $elmForm["idElm"] ?>">
                <?= $elmForm["lblText"] ?>
            </label>
        <?php endif; ?>
        </div>