<?php /* Paginatore, pulsanti per scorrere gli eventi in basso a destra */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:  
    $pagination, array associativo che contiene i dati per la paginazione:
        initPage, è il numero della prima pagina 
        pageSelected, è il tab selezionato (la "pagina" mostrata)
        pageDisabled, è l'array che indica se i 4 pulsanti sono disabilitati o meno
        lastPage, indica l'ultima pagina possibile
        getParam, è la stringa per il get da aggiungere eventualmente ai link(compresa di ?)
        page, indica la pagina per i link
 */ ?>
<?php $pagination["getParam"] = $pagination["getParam"] ?? ""; ?>
<nav class="py-3" aria-label="Elenco delle pagine degli eventi">
    <ul class="pagination justify-content-end my-0">

        <?php if ($pagination["pageDisabled"][0]) : ?>
            <li class="page-item disabled">
                <a class="page-link" href="/" aria-disabled="true">
                <?php else : ?>
            <li class="page-item">
                <a class="page-link" href="/<?= $pagination["page"] ?>/page/1<?= $pagination["getParam"] ?>">
                <?php endif; ?>
                <span class="sr-only">Prima pagina</span>
                <span>&laquo;</span>
                </a>
            </li>

            <?php for ($page = 0; $page < 3; $page++) : ?>
                <?php if ($pagination["pageDisabled"][$page + 1]) : ?>
                    <li class="page-item disabled">
                        <a class="page-link" href="/" aria-disabled="true">
                        <?php elseif ($pagination["pageSelected"] === $page + 1) : ?>
                    <li class="page-item active" aria-current="page">
                        <a class="page-link" href="/<?= $pagination["page"] ?>/page/<?= $pagination["initPage"] + $page ?><?= $pagination["getParam"] ?>"><span class="sr-only">(attuale)</span>
                        <?php else : ?>
                    <li class="page-item">
                        <a class="page-link" href="/<?= $pagination["page"] ?>/page/<?= $pagination["initPage"] + $page ?><?= $pagination["getParam"] ?>">
                        <?php endif; ?>
                        <?= $pagination["initPage"] + $page ?></a>
                    </li>
                <?php endfor; ?>

                <?php if ($pagination["pageDisabled"][4]) : ?>
                    <li class="page-item disabled">
                        <a class="page-link" href="/" aria-disabled="true">
                        <?php else : ?>
                    <li class="page-item">
                        <a class="page-link" href="/<?= $pagination["page"] ?>/page/<?= $pagination["lastPage"] ?><?= $pagination["getParam"] ?>">
                        <?php endif; ?>
                        <span class="sr-only">Ultima pagina</span>
                        <span>&raquo;</span>
                        </a>
                    </li>
    </ul>
</nav>