<?php /* Navbar presente in alto */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:  

    $nav_role, indica il ruolo di chi sta visualizzando la pagina:
        GST = ospite
        USR = utente registrato (loggato)
        ORG = utente organizzatore (loggato)
        ADM = utente admin del sistema (loggato)
    $activePage, indica l'indice della pagina attiva: 0=home, 1=Ultimi inseriti, 2=ricerca avanzata
 */?>
<nav id="main-navbar" class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">

        <a class="navbar-brand h6 mb-0" href="/">
            <img class="mr-2" src="<?= ICON_SITE . "favicon.png" ?>" width="35" height="35" alt="logo" loading="lazy">TicketHub
        </a>

        <!-- Bottone Hamburger -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-menu">
            <span class="navbar-toggler-icon"></span>
            <span class="sr-only">Menù principale</span>
        </button>
        <!-- TODO: link ricerca avanzata -->
        <div class="collapse navbar-collapse" id="navbar-menu">
            <!-- Elementi generici -->
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= isset($activePage) ? ($activePage == 0 ? "active" : "") : "" ?>">
                    <a class="nav-link" href="/">Home</a>
                </li>
                <li class="nav-item <?= isset($activePage) ? ($activePage == 1 ? "active" : "") : "" ?>">
                    <a class="nav-link" href="/last-added/">Ultimi inseriti</a>
                </li>
                <li class="nav-item <?= isset($activePage) ? ($activePage == 2 ? "active" : "") : "" ?>">
                    <a class="nav-link" href="#">Ricerca avanzata</a>
                </li>
            </ul>

            <?php if ($nav_role === "ADM" || $nav_role === "ORG") : ?>

                <!-- Elementi relativi al ruolo -->
                <ul class="navbar-nav ml-auto">

                    <?php if ($nav_role === "ORG") : ?>
                        <!-- Organizator -->
                        <!-- TODO: la tua organizzazione assente -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Organizzazione</a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="/organizations">La tua organizzazione</a>
                                <a class="dropdown-item" href="/events/create">Inserisci nuovo Evento</a>
                                <a class="dropdown-item" href="/events/list">Gestisci i tuoi Eventi</a>
                                <a class="dropdown-item" href="/tickets/validate">Convalida i Biglietti</a>
                            </div>
                        </li>
                    <?php else : ?>
                        <!-- Administrator -->
                        <!-- TODO: sistemare i link -->
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown">Amministrazione</a>
                            <div class="dropdown-menu dropdown-menu-right">
                                <a class="dropdown-item" href="/users/list">Utenti</a>
                                <a class="dropdown-item" href="/categories/list">Categorie</a>
                                <a class="dropdown-item" href="/organizations/list">Organizzazioni</a>
                                <a class="dropdown-item" href="/events/list">Eventi</a>
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
        </div>
    </div>
</nav>