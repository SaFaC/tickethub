<?php /* Menù Account */ ?>
<div class="btn-group dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
        <span class="d-none d-md-inline">Profilo</span>
        <span class="sr-only d-md-none">Profilo</span>
        <span class="mdi mdi-account-circle"></span>
    </button>
    <!-- TODO: link visualizza i tuoi ordini -->
    <div class="dropdown-menu dropdown-menu-right">
        <a class="dropdown-item" href="/users">
            <span class="mdi mdi-account-cog-outline pr-1"></span>
            Gestisci il tuo profilo</a>
        <a class="dropdown-item" href="#">
            <span class="mdi mdi-ticket-outline pr-1"></span>
            Visualizza i tuoi ordini</a>
        <a class="dropdown-item" href="/auth/logout">
            <span class="mdi mdi-exit-to-app pr-1"></span>
            Esci</a>
    </div>
</div>