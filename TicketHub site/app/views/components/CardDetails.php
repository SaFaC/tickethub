<?php /* Card della Home */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:  
    $singularEvent, l'array associativo con i dettagli dell'evento da visualizzare:
        image, indica il nome dell'immagine dell'evento da visualizzare
        title, è il titolo dell'evento
        category, è la categoria dell'evento
        description, è la descrizione estesa dell'evento
        starts_at, è la data di inziio
        seats, sono i posti disponibili
        priority_seats, sono i posti per disabili disponibili
        duration, la durata in minuti dell'evento
        address, l'indirizzo dove si trova l'evento
        price, è il prezzo del biglietto dell'evento
        event_id, è l'id dell'evento da utilizzare per il visualizza dettagli
    $page_role, indica il ruolo di chi sta visualizzando la pagina:
        GST = ospite
        USR = utente registrato (loggato)
        ORG = utente organizzatore (loggato)
        ADM = utente admin del sistema (loggato)
    $canEdit, indica se far comparire il bottone di modifica o no
 */ ?>

<!-- Dettails event -->
<?php
$dataForAddToCart =
    [
        "event_id" => $singularEvent["event_id"],
        "name" => $singularEvent["title"],
        "image" => EVENT_IMAGE . $singularEvent["image"],
        "price" => $singularEvent["price"],
        "starts_at" => $singularEvent["starts_at"]
    ];
?>
<div class="col p-3 detailCard" data-json='<?= json_encode($dataForAddToCart) ?>'>
    <article class="card bg-light border-secondary">
        <div class="row">
            <div class="col-lg-4 align-self-center">
                <img src="<?= EVENT_IMAGE . $singularEvent["image"] ?>" class="card-img" alt="locandina">
            </div>
            <div class="col-lg-8 align-self-center">
                <div class="card-body">
                    <h2 class="card-title"><?= $singularEvent["title"] ?></h2>
                    <p class="card-subtitle mb-2 text-muted">Categoria: <?= $singularEvent["category"] ?></p>
                    <p class="card-text"><?= $singularEvent["description"] ?></p>
                    <div class="row mb-3">
                        <div class="col-lg">
                            <ul class="card-text list-unstyled">
                                <li>Data evento: <?= $singularEvent["starts_at"] ?></li>
                                <li>Posti disponibili: <?= $singularEvent["seats"] ?></li>
                                <li></span>Posti prioritari (<span class="mdi mdi-wheelchair-accessibility"></span>) disponibili: <?= $singularEvent["priority_seats"] ?></li>
                            </ul>
                        </div>
                        <div class="col-lg">
                            <ul class="card-text list-unstyled">
                                <li>Durata: <?= $singularEvent["duration"] ?></li>
                                <li class="text-break">Indirizzo: <?= $singularEvent["address"] ?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col h5 m-0">
                            <p class="m-0">Prezzo: € <?= $singularEvent["price"] ?></p>
                        </div>
                        <div class="col-auto">
                            <?php if (!($page_role === "GST")) : ?>
                                <button class="btn btn-success addToCart">
                                    <span class="d-none d-md-inline">Aggiungi al carrello</span>
                                    <span class="sr-only d-md-none">Aggiungi al carrello</span>
                                    <span class="mdi mdi-cart-plus"></span>
                                </button>
                            <?php endif; ?>
                            <?php if ($canEdit) : ?>
                                <a href="/events/update/<?= $singularEvent["event_id"] ?>" class="btn btn-primary">
                                    <span class="d-none d-md-inline">Modifica</span>
                                    <span class="sr-only d-md-none">Modifica</span>
                                    <span class="mdi mdi-square-edit-outline"></span>
                                </a>
                            <?php endif; ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </article>
</div>