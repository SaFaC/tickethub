<?php /* menu Carello */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:  
    ???
 */ ?>
<?php
$userCart =
    [
        "user_id" => hash('crc32',$this->app->globalUser["email"]) ?? ""
    ];
?>
<div class="btn-group dropdown">
    <button class="btn btn-secondary dropdown-toggle" type="button" data-toggle="dropdown">
        <span class="d-none d-md-inline">Carrello</span>
        <span class="sr-only d-md-none">Carrello</span>
        <span class="mdi mdi-cart-outline"></span>
        <span class="badge badge-info" id="n-item-on-cart"></span>
    </button>
    <div class="dropdown-menu dropdown-menu-right">
        <!-- Cart items -->
        <div id="cart" class="overflow-auto" data-json='<?= json_encode($userCart) ?>'>
            <ul class="list-unstyled px-3 py-2 mb-0">

            </ul>
        </div>
        <div class="dropdown-divider"></div>
        <div class="px-4 py-3">
            <a class="btn btn-primary btn-block" href="/cart/checkout">
                Vai alla cassa
            </a>
        </div>
    </div>
</div>