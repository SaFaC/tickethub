<?php /* Footer */ ?>
<footer class="bg-dark mt-3" id="main-footer">
    <div class="container py-3">
        <p class="mb-1">
            <small>Progetto Tecnologie Web - A.A. 2019/2020</small>
        </p>
        <p class="mb-0">
            <small>Autore: Camagni Sauro</small>
        </p>
    </div>
    
</footer>