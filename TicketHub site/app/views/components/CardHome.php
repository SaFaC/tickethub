<?php /* Card della Home */ ?>
<?php /* Variabili PHP che necessita questo componente per funzionare:  
    $singularEvent, array associativo che contiene le info dell'evento
        image, indica il nome dell'immagine dell'evento da visualizzare
        title, è il titolo dell'evento
        description, è la descrizione compressa dell'evento: 112 caratteri +...
        price, è il prezzo del biglietto dell'evento
        event_id, è l'id dell'evento da utilizzare per il visualizza dettagli
 */ ?>
<div class="col-sm-6 col-md-4 p-3">
    <article class="card bg-light border-secondary h-100 event-card">
        <header>
            <img src="<?= EVENT_IMAGE . $singularEvent["image"] ?>" class="card-img-top" alt="locandina">
            <h2 class="card-header h5 mb-0 text-center"><?= $singularEvent["name"] ?></h2>
        </header>
        <div class="card-body p-2">
            <p class="card-text card-desc"><?= $singularEvent["description"] ?></p>
            <p class="card-text card-price">Prezzo: € <?= $singularEvent["price"] ?></p>
        </div>

        <footer class="card-footer bg-transparent">
            <a href="/events/details/<?= $singularEvent["event_id"] ?>" class="btn btn-primary btn-block stretched-link">Visualizza dettagli</a>
        </footer>
    </article>
</div>