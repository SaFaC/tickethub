<?php


class Controller extends BaseController
{


    public function GET_list()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM") {
            // solo l'admin può visualizzare l'elenco delle categorie
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            //carica il file che si occupa delle organizzazioni
            require_once ROOT . "/models/Organization.php";
            $organizationDB = new Organization($this->app);

            $organizations = $organizationDB->getAdmOrganizations()["data"];

            $this->render(
                "organizations",
                [
                    "page_role" => $role,
                    "organizations" => $organizations
                ],
                "defaultNoSearch"
            );
        }
    }
}
