<?php


class Controller extends BaseController
{
    //variabili per gestire i messaggi di errore da mostrare
    private $error = null;
    private $success = null;

    public function GET_list()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM") {
            // solo l'admin può visualizzare l'elenco delle categorie
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            //carica il file che si occupa delle categorie
            require_once ROOT . "/models/Category.php";
            $categoryDB = new Category($this->app);

            $data = $categoryDB->getAdmCategories()["data"];

            $this->render(
                "categories",
                [
                    "page_role" => $role,
                    "categories" => $data
                ],
                "defaultNoSearch"
            );
        }
    }


    public function GET_create()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM") {
            // solo l'admin può visualizzare l'elenco delle categorie
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            $this->render(
                "category-create_update",
                [
                    "page_role" => $role,
                    "error" => $this->error,
                    "success" => $this->success
                ],
                "defaultNoSearch"
            );
        }
    }


    public function POST_create()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM") {
            // solo l'admin può visualizzare l'elenco delle categorie
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {


            $inputData = $_POST;
            $inputData = array_map(function ($elm) {
                return htmlspecialchars($elm);
            }, $inputData);

            $checkedData = $this->checkData($inputData);

            if (!$checkedData["result"]) {
                $this->error = $checkedData["error"];
                $this->GET_create();
            } else {

                $category =
                    [
                        "name" => $inputData["category-name"],
                        "description" => $inputData["category-description"],
                        "disabled" => isset($inputData["category-disabled"]) ? true : false
                    ];

                //carica il file che si occupa delle categorie
                require_once ROOT . "/models/Category.php";
                $categoryDB = new Category($this->app);

                if ($categoryDB->existCategoryByName($category["name"])["data"]) {
                    $this->error = "Errore, la categoria inserita esiste già.";
                    $this->GET_create();
                } else {

                    if ($categoryDB->insertCategory($category)) {
                        $this->success = "Categoria creata con successo.";
                        $this->GET_create();
                    } else {
                        $this->error = "Errore durante la creazione della categoria.";
                        $this->GET_create();
                    }
                }
            }
        }
    }


    public function GET_update($category_id)
    {
        //controllo id della categoria
        $category_id = htmlspecialchars($category_id);

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM") {
            // solo l'admin può visualizzare l'elenco delle categorie
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {
            //carica il file che si occupa delle categorie
            require_once ROOT . "/models/Category.php";
            $categoryDB = new Category($this->app);


            $category = $categoryDB->getCategoryById($category_id)["data"];

            if (!$category) {
                //TODO: da genstire la categoria non trovata
                $this->redirect("/");
            } else {

                $this->render(
                    "category-create_update",
                    [
                        "page_role" => $role,
                        "error" => $this->error,
                        "success" => $this->success,
                        "value" => $category
                    ],
                    "defaultNoSearch"
                );
            }
        }
    }


    public function POST_update($category_id)
    {
        //controllo id della categoria
        $category_id = htmlspecialchars($category_id);

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM") {
            // solo l'admin può visualizzare l'elenco delle categorie
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {
            //carica il file che si occupa delle categorie
            require_once ROOT . "/models/Category.php";
            $categoryDB = new Category($this->app);

            $categoryToChange = $categoryDB->getCategoryById($category_id)["data"];

            if (!$categoryToChange) {
                //TODO: da genstire la categoria non trovata
                $this->redirect("/");
            } else {
                //prende i dati da post
                $inputData = $_POST;

                //corregge dai caratteri speciali html
                $inputData = array_map(function ($elm) {
                    return htmlspecialchars($elm);
                }, $inputData);

                $checkedData = $this->checkData($inputData);

                if (!$checkedData["result"]) {
                    $this->error = $checkedData["error"];
                    $this->GET_update($category_id);
                } else {

                    $categoryInput =
                        [
                            "name" => $inputData["category-name"],
                            "description" => $inputData["category-description"],
                            "disabled" => isset($inputData["category-disabled"]) ? true : false
                        ];

                    //array che conterrà le modifiche (differenze tra input e DB) che verranno applicate
                    $changes = [];
                    foreach ($categoryInput as $inputKey => $inputValue) {
                        if ($categoryToChange[$inputKey] != $inputValue) {
                            //se è stato inserito un valore nuovo, dentro changes inserisco gli aggiornamenti da applicare
                            $changes += [$inputKey => $inputValue];
                        }
                    }

                    if (empty($changes)) {
                        $this->error = "Nessuna modifica rilevata; non è stata apportata alcun cambiamento alla categoria.";
                        $this->GET_update($category_id);
                    } elseif ($categoryDB->updateCategory($category_id, $changes)) { //effettua l'update nel db
                        $this->success = "Categoria modificato con successo.";
                        //TODO: complicato, cercare gli eventi con quella categoria e a sua volta i biglietti di quel evento
                        //e notificare gli acquirenti della modifica effettuata alla categoria di quell'evento
                        //notifiche: invia una e-mail con i cambiamenti
                        $this->GET_update($category_id);
                    } else {
                        $this->error = "Errore durante la modifica della categoria.";
                        $this->GET_update($category_id);
                    }
                }
            }
        }
    }





    //controlla che i dati ricevuti via POST siano decenti
    //TODO: da migliorare
    private function checkData($inputData)
    {
        if (strlen($inputData["category-name"]) > 30) {
            return [
                "result" => false,
                "error" => "Errore nei dati: nome troppo lungo."
            ];
        }

        if (strlen($inputData["category-description"]) > 128) {
            return [
                "result" => false,
                "error" => "Errore nei dati: descrizione troppo lunga."
            ];
        }
        return ["result" => true];
    }
}
