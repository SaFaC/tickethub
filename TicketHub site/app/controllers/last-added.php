<?php

class Controller extends BaseController
{


    public function GET_home()
    {
        $this->GET_page(1);
    }

    //interrogazione al server per gli eventi a seconda della pagina visualizzata
    public function GET_page($page)
    {
        //controllo dela pagina
        $page = $this->checkParamId($page);

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        //variabile per il parametro di ricerca, lo prende dalla GET
        $search = isset($_GET["search"]) ? $_GET["search"] : "";

        //carica il file che si occupa degli eventi
        require_once ROOT . "/models/Event.php";
        $eventDB = new Event($this->app);

        //effettua la quuery al db con la richiesta degli eventi per quella pagina
        $response = $eventDB->selectSearchLastAddedEvents($page, $search);
        $eventsHome = $response["data"];
       
        //elaborazione variabili per la numerazione delle pagine
        {
            //estrae l'ultimo elemento dell'array associativo, che è "tot"
            $totEvents = $response["total"];

            //ultima pagina possibile
            $lastPage = intval(ceil($totEvents / $response["limit"]));

            if ($page > $lastPage) {
                //se era stata richiesta una pagina oltre il numero di pagine disponibili
                // si viene rinderizzati all'ultima pagina disponibile
                $this->redirect("/last-added/page/$lastPage");
                return;
            } elseif ($page === $lastPage) {
                //se la pagina è l'ultima possibile
                if ($page > 2) {
                    //caso generico, la pagina è l'ultima
                    $initPage = $page - 2;
                    $pageSelected = 3;
                    $pageDisabled = [false, false, false, false, true];
                } elseif ($page == 2) {
                    //caso particolare, l'ultima pagina è la 2 quindi viene mostrato al centro
                    $initPage = 1;
                    $pageSelected = 2;
                    $pageDisabled = [false, false, false, true, true];
                } else {
                    //caso particolare, l'ultima pagina è la numero 1 (unica pagina)
                    $initPage = 1;
                    $pageSelected = 1;
                    $pageDisabled = array_fill(0, 5, true);
                }
            } else {
                //se la pagina richiesta non è l'ultima
                if ($page === 1) {
                    //caso particolare, la pagina richiesta è la 1
                    $initPage = 1;
                    $pageSelected = 1;
                    if ($lastPage > 2) {
                        //se l'ultima pagina è dopo la 2 (da 3 in su)
                        $pageDisabled = [true, false, false, false, false];
                    } else {
                        //l'ultima pagina è la due
                        $pageDisabled = [true, false, false, true, false];
                    }
                } else {
                    //caso generico, la pagina sta al centro
                    $initPage = $page - 1;
                    $pageSelected = 2;
                    $pageDisabled = array_fill(0, 5, false);
                }
            }
        }
        $pagination = [
            "initPage" => $initPage,
            "pageSelected" => $pageSelected,
            "pageDisabled" => $pageDisabled,
            "lastPage" => $lastPage,
            "page" => "last-added"
        ];
        $pagination += isset($_GET["search"]) ? [
            "getParam" => "/?search=$search"
        ] : [];

        $this->render(
            "home",
            [
                "page_role" => $role,
                "eventsHome" => $eventsHome,
                "pagination" => $pagination,
                "activePage" => 1
            ],
            "defaultNoSearch"
        );
    }
}
