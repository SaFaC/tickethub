<?php

class Controller extends BaseController
{
    private $error = null;

    public function GET_login()
    {
        //pagina con il form di login
        $this->render("login", ["error" => $this->error, "page_role" => $this->app->globalUser["role"]], "defaultNoSearch");
    }

    public function POST_login()
    {
        //variabile che gestisce gli eventuali messaggi di errore
        $this->error = false;

        //corregge dai caratteri speciali html
        $_POST["em"] = htmlspecialchars($_POST["em"]??"");

        //prende i dati da post
        $inputData = $_POST;

        $checkedData = $this->checkDataLog($inputData);
        if (!$checkedData["result"]) {
            $this->error = $checkedData["error"];
        } else {

            //carica il file che si occupa dell'autenticazione
            require_once ROOT . "/models/Authentication.php";
            $authDB = new Authentication($this->app);

            if (isset($_POST['em'], $_POST['pc'])) {
                $inputEmail = htmlspecialchars($_POST['em']);
                $inputPassword = $_POST['pc']; // Recupero la password criptata.

                //cerca l'utente per email
                $user = $authDB->getUserByEmail($inputEmail);

                if ($user) { // se l'utente esiste

                    // verifichiamo che non sia disabilitato in seguito all'esecuzione di troppi tentativi di accesso errati.
                    //verifica non implementata, il metodo restituisce sempre false
                    if ($this->checkbrute($user["user_id"]) === true) {
                        // Account disabilitato
                        $this->error = "sono stati effettuati troppi tentativi di login. L'account è stato disabilitato.";
                    } else {
                        $inputPassword = hash('sha512', $inputPassword . $user["salt"]); // codifica la password usando una chiave univoca.
                        if ($user["disabled"]) {
                            //account disabilitato
                            $this->error = "impossibile effettuare il login, l'account è disabilitato.";
                        } else if ($user["password"] === $inputPassword) { // Verifica che la password memorizzata nel database corrisponda alla password fornita dall'utente.
                            // Password corretta!

                            $user["user_id"] = preg_replace("/[^0-9]+/", "", $user["user_id"]); // ci proteggiamo da un attacco XSS
                            $_SESSION['user_id'] = $user["user_id"];

                            $user["name"] = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $user["name"]); // ci proteggiamo da un attacco XSS
                            $_SESSION['name'] = $user["name"];

                            $user_browser = $_SERVER['HTTP_USER_AGENT']; // Recupero il parametro 'user-agent' relativo all'utente corrente.
                            $_SESSION['login_string'] = hash('sha512', $inputPassword . $user_browser);

                            // Login eseguito con successo.
                        } else {
                            // Password incorretta.
                            // Registriamo il tentativo fallito nel database.
                            //log dei tentativi falliti non gestito
                            // $now = time();
                            // $mysqli->query("INSERT INTO login_attempts (user_id, time) VALUES ('$user_id', '$now')");
                            $this->error = "email o password errata! Verifica i dati inseriti e riprova.";
                        }
                    }
                } else {
                    // L'utente inserito non esiste.
                    $this->error = "email o password errata! Verifica i dati inseriti e riprova.";
                }
            } else {
                // Le variabili necessarie non sono state inviate a questa pagina dal metodo POST.
                $this->error = "password non inserita.";
            }
        }

        if ($this->error) { //login non effettuato, c'è stato un errore.
            $this->GET_login();
        } else { //login eseguito. si viene reinderizzati alla Home
            $this->redirect("/");
        }
    }

    public function GET_logout()
    {
        // Elimina tutti i valori della sessione.
        $_SESSION = array();
        // Recupera i parametri di sessione.
        $params = session_get_cookie_params();
        // Cancella i cookie attuali.
        setcookie(session_name(), '', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);
        // Cancella la sessione.
        session_destroy();

        //TODO: mostrare la home con solo un alert e un messaggio di successo?
        $this->redirect("/");
    }

    public function GET_register()
    {
        //pagina con il form di registrazione
        $this->render("register", ["error" => $this->error, "page_role" => $this->app->globalUser["role"]], "defaultNoSearch");
    }

    public function POST_register()
    {
        //corregge dai caratteri speciali html
        $_POST["name"] = htmlspecialchars($_POST["name"]??"");
        $_POST["surname"] = htmlspecialchars($_POST["surname"]??"");
        $_POST["em"] = htmlspecialchars($_POST["em"]??"");

        //prende i dati da post
        $inputData = $_POST;

        $checkedData = $this->checkDataReg($inputData);
        if (!$checkedData["result"]) {
            $this->error = $checkedData["error"];
        } else {

            $this->error = false;
            // Recupero la password criptata dal form di inserimento.

            $name = $_POST["name"];
            $surname = $_POST["surname"];
            $email = $_POST["em"];
            $password = $_POST["pc"];

            if ($password !== $_POST["cpc"]) {
                //le due password non coincidono, l'utente le ha inserite male
                $this->error = "le due password inserite non corrispondono!";
            } else {
                //carica il file che si occupa dell'autenticazione
                require_once ROOT . "/models/Authentication.php";
                $authDB = new Authentication($this->app);

                // Crea una chiave casuale da usare per salare la pw
                $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));

                // Crea una password salata usando la chiave appena creata.
                $password = hash('sha512', $password . $random_salt);

                if ($authDB->getUserByEmail($email)) {
                    $this->error = "e-mail inserita è già stata utilizzata!";
                } else {
                    // Inserisci a questo punto il codice SQL per eseguire la INSERT nel tuo database
                    // Assicurati di usare statement SQL 'prepared'.
                    $user = $authDB->userRegister($name, $surname, $email, $password, $random_salt);

                    if (!$user) { //se user è false, vuol dire che l'inserimento non è andato a buon fine
                        $this->error = "problema di registrazione, riprovare più tardi o contattare l'amministratore.";
                    }
                }
            }
        }

        //va alla pagina di registrazione, che gestirà il successo o il fallimento della registrazione
        $this->GET_register();
    }

    public function GET_pwLost()
    {
        //TODO: modulo per resettare la password
    }

    public function POST_pwReset()
    {
        //TODO: invio password resettata via e-mail
    }





    private function checkbrute($user_id)
    {
        return false;
        /* 
        // Recupero il timestamp
        $now = time();
        // Vengono analizzati tutti i tentativi di login a partire dalle ultime due ore.
        $valid_attempts = $now - (2 * 60 * 60);
        if ($stmt = $mysqli->prepare("SELECT time FROM login_attempts WHERE user_id = ? AND time > '$valid_attempts'")) {
            $stmt->bind_param('i', $user_id);
            // Eseguo la query creata.
            $stmt->execute();
            $stmt->store_result();
            // Verifico l'esistenza di più di 5 tentativi di login falliti.
            if ($stmt->num_rows > 5) {
                return true;
            } else {
                return false;
            }
        }
        */
    }

    //controlla che i dati ricevuti via POST siano decenti
    //TODO: da migliorare
    private function checkDataReg($inputData)
    {
        if (!isset($inputData["name"]) || strlen($inputData["name"]) < 3) {
            return [
                "result" => false,
                "error" => "nome troppo corto (min 3)."
            ];
        }
        if (!isset($inputData["name"]) || strlen($inputData["name"]) > 30) {
            return [
                "result" => false,
                "error" => "nome troppo lungo (max 30)."
            ];
        }

        if (!isset($inputData["surname"]) || strlen($inputData["surname"]) < 3) {
            return [
                "result" => false,
                "error" => "cognome troppo corto (min 3)."
            ];
        }
        if (!isset($inputData["surname"]) || strlen($inputData["surname"]) > 30) {
            return [
                "result" => false,
                "error" => "cognome troppo lungo (max 30)."
            ];
        }

        if (strlen($inputData["em"]) > 50) {
            return [
                "result" => false,
                "error" => "email troppo lungo (max 50)."
            ];
        }
        if (strlen($inputData["em"]) == 0) {
            return [
                "result" => false,
                "error" => "email mancante."
            ];
        }

        if (!isset($inputData["pc"]) || !isset($inputData["cpc"])) {
            return [
                "result" => false,
                "error" => "password non inserita."
            ];
        }

        return ["result" => true];
    }

    //controlla che i dati ricevuti via POST siano decenti
    //TODO: da migliorare
    private function checkDataLog($inputData)
    {
        if (!isset($inputData["em"]) || strlen($inputData["em"]) == 0) {
            return [
                "result" => false,
                "error" => "email mancante."
            ];
        }
        if (!isset($inputData["em"]) || strlen($inputData["em"]) > 50) {
            return [
                "result" => false,
                "error" => "email troppo lungo (max 50)."
            ];
        }

        if (!isset($inputData["pc"])) {
            return [
                "result" => false,
                "error" => "password non inserita."
            ];
        }

        return ["result" => true];
    }
}
