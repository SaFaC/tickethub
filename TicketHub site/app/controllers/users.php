<?php


class Controller extends BaseController
{
    //variabili per gestire i messaggi di errore da mostrare
    private $error = null;
    private $success = null;
    private $isOwner;

    public function GET_list()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM") {
            // solo l'admin può visualizzare l'elenco delle categorie
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            //carica il file che si occupa degli user
            require_once ROOT . "/models/User.php";
            $userDB = new User($this->app);

            $data = $userDB->getAdmUsers()["data"];

            $this->render(
                "users",
                [
                    "page_role" => $role,
                    "users" => $data
                ],
                "defaultNoSearch"
            );
        }
    }

    //se uno richiede solamente /users/ vuol dire che sta editando il suo profilo
    public function GET_home()
    {
        $this->GET_update($this->app->globalUser["user_id"]);
    }

    public function GET_update($user_id)
    {
        //controllo id dello user
        $user_id = htmlspecialchars($user_id);

        //indica se si sta visualizzanod il proprio profilo
        $this->isOwner = $this->app->globalUser["user_id"] == $user_id;

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role === "GST") {
            // gli ospiti non possono modificare i profili
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } elseif (($role !== "ADM") && (!$this->isOwner)) {
            // gli utenti e gli organizzatori possono editare solo il loro profilo
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {
            //carica il file che si occupa degli utenti
            require_once ROOT . "/models/User.php";
            $userDB = new User($this->app);

            $user = $userDB->getUserById($user_id)["data"];

            if (!$user) {
                //TODO: da genstire l'utente non trovato
                $this->redirect("/");
            } else {

                /*
                $roles, array di array associativo (elenco dei ruoli):
                    opt_val, il valore da passare al server
                    selected, indica se è selezionato
                    opt_text, il testo associato al valore
                */
                //array contenente i ruoli e il ruolo selezionato dell'utente 
                $roles = [];
                foreach (array("USR", "ORG", "ADM") as $tempRole) {
                    $roles[] = [
                        "opt_val" => $tempRole,
                        "selected" => $user["role"] === $tempRole,
                        "opt_text" => $tempRole
                    ];
                }

                //carica il file che si occupa delle organizzazioni
                require_once ROOT . "/models/Organization.php";
                $organizationDB = new Organization($this->app);


                /*
                $organizations, array di array associativo (elenco delle organizzazioni):
                    opt_val, il valore da passare al server
                    selected, indica se è selezionato
                    opt_text, il testo associato al valore
                */

                $organizations = $organizationDB->getOrganizations()["data"];

                $temp = [];
                $user["organization"] = "";
                //aggiunge ad ogni organizzazione la l'elemento selected e effettua "ridenominazioni"
                foreach ($organizations as $org) {

                    $selected = $org["organization_id"] == $user["organization_id"];
                    //se l'utente appartiene all'organizzazione attualmente selezionata
                    if ($selected) {
                        $user["organization"] = $org["name"];
                    }
                    //"rename"
                    $org["opt_val"] = $org["organization_id"];
                    $org["opt_text"] = $org["name"];

                    $org += [
                        "selected" => $selected
                    ];
                    $temp[] = $org;
                }
                $organizations = $temp;

                $this->render(
                    "user-update",
                    [
                        "page_role" => $role,
                        "error" => $this->error,
                        "success" => $this->success,
                        "value" => $user,
                        "roles" => $roles,
                        "organizations" => $organizations,
                        "isOwner" => $this->isOwner
                    ],
                    "defaultNoSearch"
                );
            }
        }
    }

    public function POST_update($user_id)
    {
        //controllo id dello user
        $user_id = htmlspecialchars($user_id);

        //indica se si sta visualizzanod il proprio profilo
        $this->isOwner = $this->app->globalUser["user_id"] == $user_id;

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role === "GST") {
            // gli ospiti non possono modificare i profili
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } elseif (($role !== "ADM") && (!$this->isOwner)) {
            // gli utenti e gli organizzatori possono editare solo il loro profilo
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {
            //carica il file che si occupa degli utenti
            require_once ROOT . "/models/User.php";
            $userDB = new User($this->app);

            $userToChange = $userDB->getUserById($user_id)["data"];

            if (!$userToChange) {
                //TODO: da genstire l'utente non trovato
                $this->redirect("/");
            } else {

                //prende i dati da post
                $inputData = $_POST;

                //corregge dai caratteri speciali html
                $inputData = array_map(function ($elm) {
                    return htmlspecialchars($elm);
                }, $inputData);

                $checkedData = $this->checkData($inputData);
                if (!$checkedData["result"]) {
                    $this->error = $checkedData["error"];
                    $this->GET_update($user_id);
                } else {
                    if (isset($inputData["pc"]) || isset($inputData["cpc"])) {
                        //se una delle due pw è settata, controlla che siano uguali
                        if ($inputData["pc"] != $inputData["cpc"]) {
                            $this->error = "Errore, le due password inserite non combaciano.";
                            $this->GET_update($user_id);
                        } else {
                            // Crea una chiave casuale da usare per salare la pw
                            $random_salt = hash('sha512', uniqid(mt_rand(1, mt_getrandmax()), true));

                            // Crea una password salata usando la chiave appena creata.
                            $password = hash('sha512', $inputData["pc"] . $random_salt);

                            $inputData["user-password"] = $password;
                            $inputData["user-salt"] = $random_salt;
                        }
                    }

                    $userInput = [
                        "name" => $inputData["user-name"],
                        "surname" => $inputData["user-surname"]
                    ] + ($this->isOwner ?
                        ["email" => $inputData["user-email"]]
                        : [])  + (isset($inputData["user-role"]) ?
                            [ //se il ruolo è vuoto, imposta USR (anche se c'è già il check)
                                "role" => empty($inputData["user-role"]) ? "USR" : $inputData["user-role"]
                            ] : [])
                        + (isset($inputData["user-organization_id"]) ?
                            [ //se l'organizzazione è vuota, imposta null
                                "organization_id" => empty($inputData["user-organization_id"]) ? null : $inputData["user-organization_id"]
                            ] : [])
                        + (($role === "ADM") ? ["disabled" => isset($inputData["user-disabled"])] : []);


                    //se il ruolo dell'utente è ORG, vi deve essere associata un'organizzazione
                    if (isset($userInput["role"]) && ($userInput["role"] === "ORG")) {
                        if (!isset($userInput["organization_id"])) {
                            $this->error = "Errore nei dati: un organizzatore deve operare per un'organizzazione.";
                            $this->GET_update($user_id);
                            return;
                        }
                    }

                    //se all'utente è associata un'organizzazione, il suo ruolo deve essere ORG
                    if (isset($userInput["organization_id"])) {
                        if ((isset($userInput["role"]) && ($userInput["role"] !== "ORG")) || (!isset($userInput["role"]))) {
                            $this->error = "Errore nei dati: se un utente opera per un organizzazione allora è un organizzatore";
                            $this->GET_update($user_id);
                            return;
                        }
                    }


                    //array che conterrà le modifiche (differenze tra input e DB) che verranno applicate
                    $changes = [];
                    // printvar($userInput);
                    foreach ($userInput as $inputKey => $inputValue) {
                        if ($userToChange[$inputKey] != $inputValue) {
                            //se è stato inserito un valore nuovo, dentro changes inserisco gli aggiornamenti da applicare
                            $changes += [$inputKey => $inputValue];
                        }
                    }

                    //aggiunge la password e la salt se è stata inserita
                    $changes += isset($password) ? [
                        "password" => $inputData["user-password"],
                        "salt" => $inputData["user-salt"]
                    ] : [];

                    //se è stata inserita una e-mail nuova, bisogna controllare che non esista già
                    if (isset($changes["email"])) {
                        //carica il file che si occupa dell'autenticazione
                        require_once ROOT . "/models/Authentication.php";
                        $authDB = new Authentication($this->app);

                        if ($authDB->getUserByEmail($changes["email"])) {
                            $this->error = "Errore nei dati: la email inserita è occupata. Provare con un altro indirizzo email.";
                            $this->GET_update($user_id);
                            return;
                        }
                    }

                    //controlla se sono state richieste modifiche
                    if (empty($changes)) {
                        $this->error = "Nessuna modifica rilevata; non è stata apportata alcun cambiamento all'utente.";
                        $this->GET_update($user_id);
                    } elseif ($userDB->updateUser($user_id, $changes)) { //effettua l'update nel db
                        $this->success = "Modifiche effettuate consuccesso.";
                        $this->GET_update($user_id);
                    } else {
                        $this->error = "Errore durante le modifiche.";
                        $this->GET_update($user_id);
                    }
                }
            }
        }
    }



    //controlla che i dati ricevuti via POST siano decenti
    //TODO: da migliorare
    private function checkData($inputData)
    {
        if (!isset($inputData["user-name"]) || strlen($inputData["user-name"]) < 3) {
            return [
                "result" => false,
                "error" => "nome troppo corto (min 3)."
            ];
        }
        if (!isset($inputData["user-name"]) || strlen($inputData["user-name"]) > 30) {
            return [
                "result" => false,
                "error" => "Errore nei dati: nome troppo lungo (max 30)."
            ];
        }

        if (!isset($inputData["user-surname"]) || strlen($inputData["user-surname"]) < 3) {
            return [
                "result" => false,
                "error" => "cognome troppo corto (min 3)."
            ];
        }
        if (!isset($inputData["user-surname"]) || strlen($inputData["user-surname"]) > 30) {
            return [
                "result" => false,
                "error" => "Errore nei dati: cognome troppo lungo (max 30)."
            ];
        }

        if ($this->isOwner) {
            if (!isset($inputData["user-email"]) || strlen($inputData["user-email"]) == 0) {
                return [
                    "result" => false,
                    "error" => "Errore nei dati: email mancante."
                ];
            }
            if (!isset($inputData["user-email"]) || strlen($inputData["user-email"]) > 50) {
                return [
                    "result" => false,
                    "error" => "Errore nei dati: email troppa lunga (max 50)."
                ];
            }
        }

        if (isset($inputData["user-role"])) {
            if (($inputData["user-role"] !== "USR")
                && ($inputData["user-role"] !== "ORG")
                && ($inputData["user-role"] !== "ADM")
            ) {
                return [
                    "result" => false,
                    "error" => "Errore nei dati: ruolo inserito errato"
                ];
            }
        }

        return ["result" => true];
    }
}
