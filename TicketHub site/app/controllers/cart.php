<?php


class Controller extends BaseController
{


    public function GET_checkout()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];


        if ($role === "GST") {
            //un ospite non ha il carrello
            //TODO: zona proibita, si viene rimandati alla home
            $this->redirect("/");
        } else {
            $this->render(
                "checkout",
                [
                    "page_role" => $role,
                ],
                "defaultNoSearch"
            );
        }
    }



    public function POST_buy()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        //elenco degli eventi presenti nel carrello
        $cartList = $_POST["event_idList"] ?? [];

        //array che conterrà tutti i biglietti (venduti e non)
        $allTickets = [];
        //indica quanti biglietti sono stati venduti
        $nSold = 0;

        if ($role === "GST") {
            //un ospite non può effettuare acquisti
        } elseif (count($cartList) === 0) {
            //non è stato inviato nulla
        } else {

            //utente che ha effettuato l'acquisto
            $user_id = $this->app->globalUser["user_id"];

            //carica il file che si occupa dei ticket
            require_once ROOT . "/models/Ticket.php";
            $ticketDB = new Ticket($this->app);

            //carica il file che si occupa degli eventi
            require_once ROOT . "/models/Event.php";
            $eventDB = new Event($this->app);

            for ($i = 0; $i < count($cartList); $i++) {

                //trasformo le stringhe "false" o "true" in boolean
                $cartList[$i]["priority_seat"] = stringToBoolean($cartList[$i]["priority_seat"]);

                // creazione prima parte del biglietto
                $ticket = [
                    "user_id" => $user_id,
                    "event_id" => $cartList[$i]["event_id"],
                    "priority_seat" => $cartList[$i]["priority_seat"]
                ];

                //verifico se è possibile comprare un biglietto per quell'evento
                //(esistenza, abilitato, posti disponibili)
                $eventSalable = $this->isSalable($cartList[$i], $eventDB);

                if (!$eventSalable["result"]) {
                    //l'evento non trovato o disabilitato o ha esaurito i posti richiesti 
                    //mi copio il messaggio di errore
                    $ticket["error"] = $eventSalable["error"];
                } else {
                    //evento salvato sul server
                    $eventFromDB = $eventSalable["data"];

                    //evento acquistabile,
                    $ticket += [
                        // "code" => hash('md5', (time() . rand())),
                        "code" => $eventFromDB["event_id"] . "-" . hash('crc32', (time() . rand())),
                        "price" => $eventFromDB["price"],
                    ];

                    if (!$ticketDB->createTicket($ticket)) {
                        //ticket non inserito, c'è stato un problema
                        $ticket["error"] = "Errore durante la creaizone del biglietto, acquisto annullato";
                    } else {
                        // $ticket["qr-image"] =  $ticket["code"] . ".png";
                        $ticket["success"] = "Biglietto acquistato con successo.";

                        //riduce il numero dei posti disponibili e avvisa l'organizzatore se non finiti
                        $data = $this->decreaseSeat($ticket["priority_seat"], $eventFromDB, $eventDB);

                        //aggiorna l'evento dopo l'update
                        $eventFromDB = $data["data"];

                        require_once ROOT . "/utils/EmailNotifier.php";
                        $notifierChange = new EmailNotifier();

                        if (!$data["result"]) { //dei posti sono esauriti

                            //notifica il fondatore dell'organizzazione che i posti sono finiti
                            $notifierChange->notifySoldOut($eventFromDB);
                        }

                        //crea l'immagine qr-code per il biglietto
                        createQR($ticket["code"]);

                        //invia il biglietto all'acquirente
                        $notifierChange->sendTicket($ticket, $eventFromDB);

                        //incrementa il numero di biglietti venduto
                        $nSold++;
                    }
                }

                //TODO: cambiamento dell'ultimo: verificare se la parte commentata funziona
                //$allTickets[] = $ticket;
                array_push($allTickets, $ticket);
            }

            // var_dump(["result" => $allSold] + ["tickets" => $allTickets]);
            // $allSold = ($nSold === count($cartList));
            $this->json(
                [
                    "nSold" => $nSold,
                    "tickets" => $allTickets
                ]
            );
        }
    }


    //controlla se l'evento aggiunto nel carrello è vendibile e se ci sono posti a sufficienza
    private function isSalable($eventCartItem, $eventDB)
    {
        $eventFromDB = $eventDB->getEventbyId($eventCartItem["event_id"])["data"];

        if (!$eventFromDB) {
            return ["result" => false, "error" => "Evento non trovato."];
        } elseif ($eventFromDB["disabled"]) {
            return ["result" => false, "error" => "Evento annullato, non puoi acquistare un biglieto."];
        } elseif ($eventCartItem["priority_seat"]) {
            //è stato scelto un posto prioritario

            if ($eventFromDB["priority_seats"] <= 0) {
                //non ci sono posti prioritari disponibili
                return ["result" => false, "error" => "Posti prioritari esauriti, biglietto non acquistato."];
            }
        } else {
            //è stato scelto un posto normale

            if ($eventFromDB["seats"] <= 0) {
                //non ci sono posti normali disponibili
                return ["result" => false, "error" => "Posti esauriti, biglietto non acquistato."];
            }
        }

        //si può acquistare un biglietto per questo evento
        return ["result" => true, "data" => $eventFromDB];
    }

    //diminuisce il numero dei posti disponibili dell'evento
    //true se tutto ok, false in caso di esaurimento posti
    private function decreaseSeat($isPrioritySeat, $eventFromDB, $eventDB)
    {
        $seats = $isPrioritySeat ?
            ["priority_seats" => ($eventFromDB["priority_seats"] - 1)]
            :
            ["seats" => ($eventFromDB["seats"] - 1)];

        $afterUpdate = $eventDB->updateEvent($eventFromDB["event_id"], $seats);

        //controlla se i posti si sono azzerati
        if ($afterUpdate["seats"] <= 0 || $afterUpdate["priority_seats"] <= 0) {
            //posti finiti
            return ["result" => false, "data" => $afterUpdate];
        } else {
            //tutto a posto
            return ["result" => true, "data" => $afterUpdate];
        }
    }
}
