<?php

class Controller extends BaseController
{
    //variabili per gestire i messaggi di errore da mostrare
    private $error = null;
    private $success = null;

    public function GET_details($event_id)
    {
        //controllo id dell'evento
        $event_id = $this->checkParamId($event_id);

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        //carica il file che si occupa degli eventi
        require_once ROOT . "/models/Event.php";
        $eventDB = new Event($this->app);

        $singularEvent = $eventDB->getEventById($event_id)["data"];

        if (!$singularEvent) {
            //TODO: da genstire l'evento non trovato
            $this->redirect("/");
        } else {

            $user_id = $this->app->globalUser["user_id"] ?? "";

            //controlla se si ha il permesso di edit dell'evento
            $canEdit = $this->canEdit($eventDB, $user_id, $event_id);

            $this->render(
                "event-details",
                [
                    "singularEvent" => $singularEvent,
                    "page_role" => $role,
                    "canEdit" => $canEdit
                ],
                "defaultNoSearch"
            );
        }
    }


    public function GET_create()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        // if ($role !== "ORG" && $role !== "ADM") {
        if ($role !== "ORG") {
            //solo un organizzatore puo' creare un evento
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            //carica il file che si occupa delle categorie
            require_once ROOT . "/models/Category.php";
            $categoryDB = new Category($this->app);

            $data = $categoryDB->getCategories();
            $categories = $data["data"];

            //aggiunge ad ogni categoria la l'elemento selected=>false
            $categories = array_map(function ($cat) {

                $cat["opt_val"] = $cat["category_id"];
                $cat["opt_text"] = $cat["name"];
                return $cat += ["selected" => false];
            }, $categories);

            $this->render(
                "event-create_update",
                [
                    "page_role" => $role,
                    "error" => $this->error,
                    "success" => $this->success,
                    "categories" => $categories
                ],
                "defaultNoSearch"
            );
        }
    }


    public function POST_create()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        // if ($role !== "ORG" && $role !== "ADM") {
        if ($role !== "ORG") {
            //solo un organizzatore puo' creare un evento
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            //carica il file che si occupa degli eventi
            require_once ROOT . "/models/Event.php";
            $eventDB = new Event($this->app);

            $data = $_POST;
            $data = array_map(function ($elm) {
                return htmlspecialchars($elm);
            }, $data);

            $_FILES["event-file"]["name"] = str_replace(" ", "_", htmlspecialchars($_FILES["event-file"]["name"]));
            $data += $_FILES;

            if (!$this->checkData($data)) {
                //$this->error = "Errore di validazione dei dati.";
                $this->GET_create();
            } else {

                //nome del file della immagine: "tempo in secondi"-"nome del file", così si pososno salvare file con lo stesso nome
                $data["event-file"]["name"] = strval(time()) . "-" . $data["event-file"]["name"];
                //salva l'immagine nella cartella delle imagini degli eventi
                $this->uploadImage($data["event-file"]);

                $event =
                    [
                        "name" => $data["event-title"],
                        "image" => $data["event-file"]["name"],
                        "description" => $data["event-description"],
                        "seats" => $data["event-seats"],
                        "priority_seats" => $data["event-priority-seats"],
                        "price" => $data["event-price"],
                        "starts_at" => $data["event-startdate"],
                        "ends_at" => $data["event-enddate"],
                        "address" => $data["event-address"],
                        "category_id" => $data["event-category"],
                        "organization_id" => $this->app->globalUser["organization_id"]
                    ];

                //richiama la insert nel db per inserire l'evento
                if ($eventDB->insertEvent($event)) {
                    $this->success = "Evento creato con successo.";
                    $this->GET_create();
                } else {
                    $this->error = "Errore durante la creazione dell'evento.";
                    $this->GET_create();
                }
            }
        }
    }


    public function GET_update($event_id)
    {
        //controllo id dell'evento
        $event_id = $this->checkParamId($event_id);

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ORG" && $role !== "ADM") {
            //solo  un ORG o un ADM possono modificare gli eventi
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            //carica il file che si occupa degli eventi
            require_once ROOT . "/models/Event.php";
            $eventDB = new Event($this->app);

            $data = $eventDB->getEventById($event_id);
            $event = $data["data"];

            if (!$event) {
                //TODO: da genstire l'evento non trovato
                $this->redirect("/");
            } else {

                if (($role === "ORG")
                    && ($this->app->globalUser["organization_id"] !== $event["organization_id"]
                        || boolval($event["disabled"]))
                ) {
                    //controllo se è un organizzatore che non può apportare modifiche:
                    //se è un organizzatore
                    // e anche
                    //( non appartiene alla stessa organizzazione responsabile dell'evento
                    //oppure l'evento è disabilitato )

                    //TODO: zona proibita, per ora si viene rimandati alla home
                    $this->redirect("/");
                } else {
                    //l'utente può modificare l'evento

                    //TODO: gestire meglio le date... (non come stringhe)
                    $event["starts_at"] = str_replace(" ", "T", $event["starts_at"]);
                    $event["ends_at"] = str_replace(" ", "T", $event["ends_at"]);

                    //carica il file che si occupa delle categorie
                    require_once ROOT . "/models/Category.php";
                    $categoryDB = new Category($this->app);

                    //richiesta di tutte le categorie
                    $data = $categoryDB->getCategories();
                    $categories = $data["data"];

                    //richiesta della categoria dell'evento e ricerca dell'indice nell'elenco completo
                    $data = $categoryDB->getCategoryById($event["category_id"]);
                    $category = $data["data"];
                    $ind = array_search($category, $categories);

                    //aggiunge ad ogni categoria la l'elemento selected=>false
                    $categories = array_map(function ($cat) {
                        $cat["opt_val"] = $cat["category_id"];
                        $cat["opt_text"] = $cat["name"];
                        $cat += ["selected" => false];
                        return $cat;
                    }, $categories);

                    //modifica la proprietà selected a true della categoria dell'evento
                    $categories[$ind]["selected"] = true;

                    $this->render(
                        "event-create_update",
                        [
                            "page_role" => $role,
                            "error" => $this->error,
                            "success" => $this->success,
                            "categories" => $categories,
                            "value" => $event
                        ],
                        "defaultNoSearch"
                    );
                }
            }
        }
    }


    public function POST_update($event_id)
    {
        //controllo id dell'evento
        $event_id = $this->checkParamId($event_id);

        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ORG" && $role !== "ADM") {
            //solo  un ORG o un ADM possono modificare gli eventi
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {

            //carica il file che si occupa degli eventi
            require_once ROOT . "/models/Event.php";
            $eventDB = new Event($this->app);

            $data = $eventDB->getEventById($event_id);
            //eventToChange contiene le info dell'evento da modificare dal DB
            $eventToChange = $data["data"];

            if (!$eventToChange) {
                //TODO: da genstire l'evento non trovato
                $this->redirect("/");
            } else {

                if (($role === "ORG")
                    && ($this->app->globalUser["organization_id"] !== $eventToChange["organization_id"]
                        || boolval($eventToChange["disabled"]))
                ) {
                    //controllo se è un organizzatore che non può apportare modifiche:
                    //se è un organizzatore
                    // e anche
                    //( non appartiene alla stessa organizzazione responsabile dell'evento
                    //oppure l'evento è disabilitato )

                    //TODO: zona proibita, per ora si viene rimandati alla home
                    $this->redirect("/");
                } else {

                    //TODO: gestire meglio le date... (non come stringhe)
                    $eventToChange["starts_at"] = str_replace(" ", "T", $eventToChange["starts_at"]);
                    $eventToChange["ends_at"] = str_replace(" ", "T", $eventToChange["ends_at"]);

                    $data = array_map(function ($elm) {
                        if (is_string($elm)) {
                            return htmlspecialchars($elm);
                        } else {
                            return $elm;
                        }
                    }, $data);

                    //verifica se è stata inserita una nuova immagine
                    $newImage = !empty($_FILES["event-file"]["name"]);

                    $data = $_POST;
                    if ($newImage) {
                        $_FILES["event-file"]["name"] = str_replace(" ", "_", htmlspecialchars($_FILES["event-file"]["name"]));
                        $data += $_FILES;
                    }
                    if (!$this->checkData($data)) {

                        //$this->error = "Errore di validazione dei dati.";
                        $this->GET_update($event_id);
                    } else {


                        $eventInput =
                            [
                                "name" => $data["event-title"],
                                "category_id" => $data["event-category"],
                                "description" => $data["event-description"],
                                "seats" => $data["event-seats"],
                                "priority_seats" => $data["event-priority-seats"],
                                "price" => $data["event-price"],
                                "starts_at" => $data["event-startdate"],
                                "ends_at" => $data["event-enddate"],
                                "address" => $data["event-address"],
                                // "organization_id" => $this->app->globalUser["organization_id"]
                            ];

                        if ($newImage) {
                            //se è stata inserita un'immagine, la salva

                            $data["event-file"]["name"] = strval(time()) . "-" . $data["event-file"]["name"];
                            //salva l'immagine nella cartella delle imagini degli eventi
                            $this->uploadImage($data["event-file"]);

                            $eventInput +=
                                [
                                    "image" => $data["event-file"]["name"]
                                ];
                        }

                        if ($role === "ADM") {
                            $eventInput +=
                                [
                                    "disabled" => isset($data["event-disabled"]) ? true : false
                                ];
                        }

                        //array che conterrà le modifiche (differenze tra input e DB) che verranno applicate
                        $changes = [];
                        foreach ($eventInput as $inputKey => $inputValue) {
                            if ($eventToChange[$inputKey] != $inputValue) {
                                //se è stato inserito un valore nuovo, dentro changes inserisco gli aggiornamenti da applicare
                                $changes += [$inputKey => $inputValue];
                            }
                        }

                        if (empty($changes)) {
                            $this->error = "Nessuna modifica rilevata; non è stata apportata alcun cambiamento all'evento.";
                            $this->GET_update($event_id);
                        } elseif ($eventDB->updateEvent($event_id, $changes)) { //effettua l'update nel db
                            $this->success = "Evento modificato con successo. Verrà inviata una e-mail a coloro che hanno acquistato un biglietto dell'evento indicante gli aggiornamenti.";

                            //notifiche: invia una e-mail con i cambiamenti

                            require_once ROOT . "/utils/EmailNotifier.php";
                            $notifierChange = new EmailNotifier();
                            $notifierChange->notifyTheChanges($eventToChange, $changes);

                            $this->GET_update($event_id);
                        } else {
                            $this->error = "Errore durante la modifica dell'evento.";
                            $this->GET_update($event_id);
                        }
                    }
                }
            }
        }
    }


    public function GET_list()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ADM" && $role !== "ORG") {
            // solo l'admin e l'organizzatore può visualizzare l'elenco degli eventi
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } elseif ($role === "ADM") {
            //è l'ADM, può vedere tutto

            //carica il file che si occupa degli eventi
            require_once ROOT . "/models/Event.php";
            $eventDB = new Event($this->app);

            $events = $eventDB->getAdmEvents()["data"];

            $this->render(
                "events",
                [
                    "page_role" => $role,
                    "events" => $events
                ],
                "defaultNoSearch"
            );
        } else {
            //è l'ORG, può vedere solo i suoi eventi della sua organizzazione

            //carica il file che si occupa delle organizzazioni
            require_once ROOT . "/models/Event.php";
            $eventDB = new Event($this->app);

            $events = $eventDB->getOrgEvents($this->app->globalUser["organization_id"])["data"];

            $this->render(
                "events",
                [
                    "page_role" => $role,
                    "events" => $events
                ],
                "defaultNoSearch"
            );
        }
    }


    private function canEdit($eventDB, $user_id, $event_id)
    {

        $role = $this->app->globalUser["role"];
        if ($role == "ADM") {
            $canEdit = true;
        } else {
            $canEdit = $eventDB->isOwner($user_id, $event_id);
        }
        return $canEdit;
    }


    //controlla che i dati ricevuti via POST siano decenti
    //TODO: da migliorare
    //aggiungere i controlli sulle date e sulle categorie
    private function checkData($data)
    {
        if (strlen($data["event-title"]) > 50) {
            $this->error = "Errore nei dati: titolo troppo lungo.";
            return false;
        }
        //check sul file, se è un'immagine
        //(in update non è richiesta la immagine) 
        if (!empty($data["event-file"]["name"])) {
            if (!$this->checkImg($data["event-file"])) {
                $this->error = "Errore nei dati: immagine non corretta.";
                return false;
            }
        }
        if (strlen($data["event-description"]) > 1000) {
            $this->error = "Errore nei dati: descrizione troppo lunga.";
            return false;
        }
        if (strlen($data["event-address"]) > 250) {
            $this->error = "Errore nei dati: indirizzo troppo lungo.";
            return false;
        }
        if (is_numeric($data["event-seats"])) {
            if (($data["event-seats"] < 0) || ($data["event-seats"] > 5000)) {
                $this->error = "Errore nei dati: numero di posti errato.";
                return false;
            }
        } else {
            $this->error = "Errore nei dati: inserire un numero di posti corretto.";
            return false;
        }
        if (is_numeric($data["event-priority-seats"])) {
            if (($data["event-priority-seats"] < 0) || ($data["event-priority-seats"] > 5000)) {
                $this->error = "Errore nei dati: numero di posti prioritari errato.";
                return false;
            }
        } else {
            $this->error = "Errore nei dati: inserire un numero di posti prioritari corretto.";
            return false;
        }
        if (is_numeric($data["event-price"])) {
            if ($data["event-price"] < 0) {
                $this->error = "Errore nei dati: prezzo non corretto.";
                return false;
            }
        } else {
            $this->error = "Errore nei dati: prezzo non corretto.";
            return false;
        }
        /*
        TODO: non controllati
        ["event-category"] => int
        ["event-startdate"]=> "2020-11-04T09:00"
        ["event-enddate"]=> "2020-11-04T14:00"
        */
        return true;
    }


    private function checkImg($img)
    {
        //recupera il percorso temporaneo del file
        $userfile_tmp = $img['tmp_name'];

        //recupero il nome originale del file caricato
        $userfile_name = $img['name'];

        // verifica che il file sia stato effettivamente caricato
        if (!isset($img) || !is_uploaded_file($userfile_tmp)) {
            // 'Non hai inviato nessun file...'
            return false;
        }

        // limita la dimensione massima a 4MB
        //2 MB = 2097152
        //4 MB = 4194304
        if ($img['size'] > 4194304) {
            // 'Il file è troppo grande!'
            return false;
        }

        //verifica sull'estensione
        $ext_acepted = array('png', 'jpg', 'jpeg', 'bmp');
        $eplode = explode('.', $userfile_name);
        $ext_file = end($eplode);
        if (!in_array($ext_file, $ext_acepted)) {
            // 'Il file ha un estensione non ammessa!'
            return false;
        }

        //se è un immagine, estrapola le informazioni
        $is_img = getimagesize($userfile_tmp);
        //verifica se è un immagine
        if (!$is_img) {
            // 'Puoi inviare solo immagini'
            return false;
        }

        return true;
    }


    private function uploadImage($img)
    {
        //Recupero il percorso temporaneo del file
        $userfile_tmp = $img['tmp_name'];

        //percorso della cartella delle immagini degli eventi
        $uploaddir = ROOT . "/.." . EVENT_IMAGE;

        //percorso completo di destinazione
        $target_file = $uploaddir . $img['name'];

        //copia il file dalla sua posizione temporanea alla cartella delle immagini degli eventi
        if (move_uploaded_file($userfile_tmp, $target_file)) {
            //l'operazione è andata a buon fine
            return true;
        } else {
            //l'operazione è fallta
            return false;
        }
    }
}
