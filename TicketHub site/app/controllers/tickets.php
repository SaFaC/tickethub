<?php


class Controller extends BaseController
{


    public function GET_validate()
    {
        //estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];

        if ($role !== "ORG") {
            // solo l'organizzatore può validare i biglietti
            //TODO: zona proibita, per ora si viene rimandati alla home
            $this->redirect("/");
        } else {
            $this->render(
                "validate",
                [
                    "page_role" => $role,
                ],
                "defaultNoSearch"
            );
        }
    }


    public function POST_checkCode()
    {

        // estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];
        $ticket = [];

        if ($role !== "ORG") {
            // solo l'organizzatore può validare i biglietti
            $ticket = ["result" => false];
        } elseif (!isset($_POST["code"])) {
            //non è stato inviato nulla
            $ticket = ["result" => false];
        } else {
            $code = $_POST["code"];
            //carica il file che si occupa dei ticket
            require_once ROOT . "/models/Ticket.php";
            $ticketDB = new Ticket($this->app);

            $ticket = $ticketDB->getVerboseTicket($code)["data"];

            if (!$ticket) {
                //nessun biglietto è stato trovato
                $ticket = [
                    "result" => false,
                    "message" => "Codice del biglietto inserito non valido."
                ];
            } elseif ($ticket["validated"]) {
                //il biglietto è già stato validato
                $ticket = [
                    "result" => false,
                    "message" => "Il biglietto con il codice inserito è già stato validato!"
                ];
            } else {
                $ticket = [
                    "result" => true,
                    "message" => "ok"
                ] + $ticket;
            }
        }

        $this->json($ticket);
    }

    public function POST_validate()
    {
        // estrapolazione ruolo utente
        $role = $this->app->globalUser["role"];
        $result = [];

        if ($role !== "ORG") {
            // solo l'organizzatore può validare i biglietti
            $result = [
                "result" => false,
                "title" => "Errore di autorizzazione.",
                "text" => "Non hai i permessi per validare i biglietti."
            ];
        } elseif (!isset($_POST["code"])) {
            //non è stato inviato nulla
            $result = [
                "result" => false,
                "title" => "Errore di invio dati.",
                "text" => "Il server non ha ricevuto nessun codice biglietto da convalidare."
            ];
        } else {

            $code = $_POST["code"];
            //carica il file che si occupa dei ticket
            require_once ROOT . "/models/Ticket.php";
            $ticketDB = new Ticket($this->app);

            $data = $ticketDB->getVerboseTicket($code)["data"];

            if (!$data) {
                //nessun biglietto è stato trovato, biglietto errato
                $result = [
                    "result" => false,
                    "title" => "Errore del biglietto.",
                    "text" => "Non è stato trovato nessun biglietto con il codice inserito."
                ];
            } elseif ($data["validated"]) {
                //il biglietto è già stato validato
                $result = [
                    "result" => false,
                    "title" => "Errore del biglietto.",
                    "text" => "Il biglietto con il codice inserito è già stato validato!"
                ];
            } else {

                $data = $ticketDB->validateTicket($data["ticket_id"])["success"];

                if (!$data) {
                    //errore durante l'update
                    $result = [
                        "result" => false,
                        "title" => "Errore durante la validazione.",
                        "text" => "C'è stato un problema durante la validazione; riprovare."
                    ];
                } else {
                    //update effettuato
                    $result = [
                        "result" => true,
                        "title" => "Validazione effettuata con successo!",
                        "text" => "Il biglietto è stato validato alla data attuale."
                    ];
                }
            }
        }

        $this->json($result);
    }
}
