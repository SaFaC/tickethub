<?php

class BaseController
{
    protected $app;

    public function __construct($app)
    {
        $this->app = $app;
    }

    public function redirect($url)
    {
        header("location: $url");
    }

    protected function json($data) //array associativo
    {
        header("Content-type: application/json");
        print json_encode($data);
    }

    protected function render($view = "home", $props = [], $layout = "default")
    {
        extract($props); //estrae le variabili dall'array associativo e le inserisce nel contesto attuale

        if ($layout !== "component") {

            //construisce il main della pagina
            ob_start(); //tutte le stampe vengno bufferizzate da parte e non mostrate
            require ROOT . "/views/main_content/$view.php";
            $main = ob_get_contents(); //restituisce tutte le stampe bufferizzate
            ob_end_clean(); //ristabilisce il normale flusso di output

            //carica la pagina e inserisce il $main e i vari componenti (vedi all'interno)
            require ROOT . "/views/layouts/$layout.php";
        } else {
            //carica il singolo componente
            require ROOT . "/views/components/$view.php";
        }
    }

    //restituisce il parametro in int se è valido
    protected function checkParamId($param)
    {
        //controlla se l'id passato è un intero positivo
        if ($param <= 0) {
            $this->redirect("/");
            die();
        }
        return intval($param);
    }
}
