<?php

class BaseModel
{
    protected $dbh;
    protected $table;

    public function __construct($app, $table)
    {
        $this->dbh = $app->dbh;
        $this->table = $table;
    }

    // INSERT GENERICO
    protected function insert($data)
    {

        $keys = implode(", ", array_keys($data));
        // $values = implode(", :", array_keys($data));
        $values = implode(
            ", ",
            array_map(function ($i) {
                return ":$i"; //segnaposto
            }, array_keys($data))
        );
        $query =    "INSERT INTO $this->table($keys) 
                    VALUES ($values)";

        $id_success = $this->dbh->insert($query, $data);

        //se ha avuto successo, in id_success c'è l'id e passa la condizione
        //cerca o meno l'ultima riga inserita
        $res = empty($id_success) ? false :  $this->selectForId($id_success);

        return $res;
    }

    // UPDATE
    protected function update($id, $data)
    {
        $params = implode(
            ", ",
            array_map(function ($i) {
                return "$i=:$i"; //"nome_colonna" = "segnaposto_valore" 
            }, array_keys($data))
        );

        $query =   "UPDATE $this->table 
                    SET $params 
                    WHERE " . $this->table . "_id = :idUpdate";
        $success = $this->dbh->update($query, $data + ["idUpdate" => $id]);

        //cerca o meno la riga aggiornata
        $res = empty($success) ? false : $this->selectForId($id);

        return $res;
    }

    // SELECT PER ID
    protected function selectForId($id)
    {

        $query =    "SELECT * 
                    FROM $this->table 
                    WHERE " . $this->table . "_id = ?";
        $res = $this->dbh->select($query, [$id]);

        return empty($res) ? false : $res[0]; //$res è un array di righe, ma ne viene restituita solo una
    }
}
