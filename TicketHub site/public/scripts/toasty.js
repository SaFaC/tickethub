function toasty(title, text) {
  const id = `toasty-${Math.random().toString(36).substr(2, 9)}`;
  const html = `
    <div id="${id}" role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false">
      <div class="toast-header">
        <strong class="mr-auto">${title}</strong>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="toast-body">
        ${text}
      </div>
    </div>
  `;

  $("body").append(html);
  $(`#${id}`).toast("show");

  setTimeout(() => $(`#${id}`).toast("dispose"), 3000);
  setTimeout(() => $(`#${id}`).remove(), 4000);
}
