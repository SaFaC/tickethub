//Script per la gestione del carrello in tutto il sito web

/*
esempio di oggetto cart
cart =
{
    user: 1,
    items:
        [
            {
                event_id: 43,
                name: "Fiera della probabilità",
                image: "/public/images/events_image/1598819796-dado2.png",
                price: 0,
                starts_at: "2020-09-12 09:00:00",
                priority_seat : false;
            },
            {
                event_id: 3,
                name: "Visita Fortezza di Sarzanello",
                image: "/public/images/events_image/fortezza_sarzanello_320.jpg",
                price: 4.4,
                starts_at: "2020-09-15 10:00:00",
                priority_seat: false
            }
        ]
};
*/
//carrello dell'utente
cart =
{
    user: "",
    items: []
};

$(function () {

    //se non si è loggati l'ospite non ha il carrello (e non lo vede)
    if ($("#cart").length) {
        //se si è loggati, compare il carrello; estrapolo l'id utente
        cart.user = JSON.parse($("#cart").attr("data-json")).user_id;

        //carica il carrello salvato in locale (se presente)
        loadItems();

        //associa la funziona addItem al bottone "aggiungi al carrello"
        //(anchesso presente solo se si è loggati)
        $(".addToCart").click(addItem);

        $("#cart-list-footer button:first-child").click(removeAllItems);
        $("#cart-list-footer button:nth-child(2)").click(buyItems);
    }

});

//carica gli elementi dal carrello locale
function loadItems() {

    //legge il carrello (se presente) sotto forma di oggetto
    let localCart = JSON.parse(localStorage.getItem(cart.user));

    //se il carrello dell'utente non è vuoto
    if (localCart !== null) {

        //salva il carrello caricato
        cart.items = localCart.items;

    }

    //aggiorna il carrello nell'html
    updateHtmlCart();
}


//aggiunge un elemento nel carrello
function addItem() {

    //estrapola la stringa formato json e lo trasforma in oggetto
    let itemCart = JSON.parse($(".detailCard").attr("data-json"));

    //di default non è selezionato il posto prioritario
    itemCart.priority_seat = false;

    //inserisco l'oggetto nel carrello
    cart.items.push(itemCart);

    //salvo il carrello
    localStorage.setItem(cart.user, JSON.stringify(cart));

    //aggiorna il carrello nell'html
    updateHtmlCart();

    //visualizza la notifica per l'aggiunta sul carrello
    toasty("Successo!","Hai aggiunto il biglietto nel carrello.");
}


//rimuove un elemento dal carrello
function removeItem(indexItem) {

    cart.items.splice(indexItem, 1);

    //salvo il carrello
    if (cart.items.length !== 0) {
        localStorage.setItem(cart.user, JSON.stringify(cart));
    } else {
        //il carrello è vuoto, lo cancello dalla memoria locale
        localStorage.removeItem(cart.user);
    }

    //aggiorna il carrello nell'html
    updateHtmlCart();
}


//rimuove tutti gli elementi dal carrello
function removeAllItems() {
    cart.items = [];
    localStorage.removeItem(cart.user);

    //aggiorna il carrello nell'html
    updateHtmlCart();
}


//cambia la scelta del posto prioritario
function selectPrioritySeat(index) {
    cart.items[index].priority_seat = !cart.items[index].priority_seat;
    localStorage.setItem(cart.user, JSON.stringify(cart));
}


//effettua l'acquisto dei biglietti
function buyItems() {

    // //estrapola un array con soli gli id degli eventi e se è per un posto prioritario
    let event_idList = cart.items.map(item => ({
        event_id: item.event_id,
        priority_seat: item.priority_seat
    }));

    if (event_idList.length == 0) {
        //carrello vuoto, qualcuno ha abilitato il pulsante...
        alert("Non giocare con l'HTML ;)");
        return;
    }

    //invia gli id tramite POST
    $.post("/cart/buy", { event_idList }, function (ticketList) {

        //riabilita i due pulsanti
        toggleButton(true);

        //memorizza il messaggio da mostrare
        let alert;

        //è l'ora attuale
        let time = new Date().toLocaleTimeString()

        if (ticketList.nSold === cart.items.length) {
            //tutti i biglietti sono stati venduti
            removeAllItems();

            alert = buildAlert(
                "success",
                `[${time}] Aquisto effettuato con successo!`,
                "Riceverai via email i biglietti da te acquistati.");
        } else if (ticketList.nSold === 0) {
            //nessun biglietto è stato venduto

            alert = buildAlert("danger",
                `[${time}] Errore durante la vendita dei biglietti.`,
                "I biglietti rimanenti nel carrello non sono stati acquistati (posti esauriti / evento non più disponibile)");
        } else {
            //alcuni biglietti non sono stati venduti, i biglietti invenduti vengono lasciati nel carrello
            for (let i = ticketList.tickets.length - 1; i >= 0; i--) {
                if (ticketList.tickets[i].hasOwnProperty("success")) {
                    //rimuovo dal carrello gli eventi venduti
                    removeItem(i);
                }
            }

            alert = buildAlert("warning",
                `[${time}] Errore durante la vendita di alcuni biglietti.`,
                "Riceverai via email i biglietti da te acquistati. I biglietti rimanenti nel carrello non sono stati acquistati (posti esauriti / evento non più disponibile)");
        }



        //rimuove eventuali messaggi di alert precedenti
        $("#cart-list>ul li[class^='alert']").remove();

        //inserisce il messaggio nuovo
        $("#cart-list>ul").prepend(alert);

    });

    //disabilita i due pulsanti e fa comparire la spin
    toggleButton(false);
}


//Metodi funzionali

//aggiorna l'html del carrello
function updateHtmlCart() {

    //conterrà il carrello in formato html
    let htmlCart = "";

    let cartSize = cart.items.length;
    //conterrà il carrello in formato html per la pagina di riepilogo
    let htmlPageCart = "";

    //conterrà il totale in euro del carrello
    let totaleEuro = 0;

    if (cartSize == 0) {
        //aggiungo l'indicazione che il carrello è vuoto
        htmlCart =
            `<li class="rounded bg-light p-2 mb-2 text-center">
                Il carrello è vuoto!
            </li>`;

        htmlPageCart = buildAlert("info", "Il carrello è vuoto!");
    } else {
        for (let index = 0; index < cart.items.length; index++) {
            htmlCart += buildHtmlCart(index);
            htmlPageCart += buildHtmlPageCart(index);
            totaleEuro += cart.items[index]["price"];
        }
    }

    //aggiunge il carrello html
    $("#cart>ul").html(htmlCart);

    //carica il numero di elementi nell'html
    $("#n-item-on-cart").text(cart.items.length);

    //se è presente l'elemento cart-list, si sta visualizzando la pagina di riepilogo
    if ($("#cart-list").length) {
        $("#cart-list>ul").html(htmlPageCart);
        //se il carrello è vuoto, disabilito i pulsanti dell'aquisto e del svuota tutto
        if (cartSize === 0) {
            $("#cart-list-footer button").prop("disabled", true);
        }else{
            //visualizzo il totale
            $("#total-price").html(`
            <hr class="m-0">
                <div class="text-right py-2 my-1 px-3 mx-1 font-weight-bold h6">
                    Totale nel carrello: € ${totaleEuro}
                </div>`
            );

        }
    }
}


//costruisce l'oggetto del carrello
function buildHtmlCart(index) {
    let item = cart.items[index];
    return `<li class="cart-media media border rounded bg-light p-2 mb-2">
            <img src="${item.image}"class="cart-img align-self-center mr-2" alt="locandina" />
            <section class="media-body">
                <h2 class="mt-0 mb-1 h6">${item.name}</h2>
                <div class="d-flex align-items-end">
                    <ul class="list-unstyled">
                        <li>Prezzo: € ${item.price}</li>
                        <li>Data: ${item.starts_at}</li>
                    </ul>
                    <button class="btn btn-danger btn-sm py-0 px-1 ml-auto" onclick="removeItem(${index})" >
                        <span class="mdi mdi-delete"></span>
                        <span class="sr-only">Rimovi dal carrello</span>
                    </button>
                </div>
            </section>
        </li>`;
}


//costruisce l'oggetto del carrello per la pagina di riepilogo
function buildHtmlPageCart(index) {
    let item = cart.items[index];
    let checked = item["priority_seat"] ? "checked" : "";
    return `<li class="border rounded bg-light p-2 mb-3">
                <div class="d-sm-flex align-items-end">
                    <div class="media">
                        <img src="${item.image}" class="cart-img align-self-center mr-2" alt="" />
                        <section class="media-body">
                            <h2 class="mt-0 mb-1 h6">${item.name}</h2>
                            <ul class="list-unstyled">
                                <li>Prezzo: € ${item.price}</li>
                                <li>Data: ${item.starts_at}</li>
                            </ul>
                        </section>
                    </div>

                    <div class="ml-sm-auto text-right">
                        <input type="checkbox" id="chkSpecialSeat_${index}" onclick="selectPrioritySeat(${index})" ${checked} />
                        <label for="chkSpecialSeat_${index}">posto prioritario (<span class="mdi mdi-wheelchair-accessibility"></span>)</label>
                        <div>
                            <a href="/events/details/${item.event_id}" class="btn btn-primary btn-sm py-0 px-1 mr-1">
                                <span class="d-none d-sm-inline">Visualizza</span>
                                <span class="sr-only d-sm-none">Visualizza</span>
                                <span class="mdi mdi-eye"></span>
                            </a>
                            <button class="btn btn-danger btn-sm py-0 px-1 ml-1" onclick="removeItem(${index})">
                                <span class="d-none d-sm-inline">Rimuovi</span>
                                <span class="sr-only d-sm-none">Rimuovi</span>
                                <span class="mdi mdi-delete"></span>
                            </button>
                        </div>
                    
                    </div>

                </div>

            </li>`;

}


//costruisce i messaggi alert
function buildAlert(type, title, text = "") {
    return `<li class="alert alert-${type} text-center mb-3 p-2"><h2 class="h5">${title}</h2>${text}</li>`;
}


//abilita o disabilita i pulsanti nella pagina del carrello
function toggleButton(active) {

    //disattiva/attiva i pulsanti
    $("#cart-list-footer button").prop("disabled", !active);

    //cambia l'icona del pulsante acquista (cart o spin)
    let spin = "spinner-border spinner-border-sm";
    let cart = "mdi mdi-cart-arrow-right";
    $("#cart-list-footer button:nth-child(2)>span:nth-child(3)").toggleClass(cart + " " + spin);

    //cambia il testo dei pulsanti
    if (active) {
        $("#cart-list-footer button:nth-child(2)>span:nth-child(1)").text("Acquista");
        $("#cart-list-footer button:nth-child(2)>span:nth-child(2)").text("Acquista");
    } else {
        $("#cart-list-footer button:nth-child(2)>span:nth-child(1)").text("Loading...");
        $("#cart-list-footer button:nth-child(2)>span:nth-child(2)").text("Acquisto in caricamento");
    }
}