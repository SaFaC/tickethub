//script per la convalida dei biglietti

//componente per lo scanner
var html5QrcodeScanner;

//variabile del codice
var code = "";

$(function () {

    //verifica se si è nella pagina della verifica ticket
    if ($("#main-validate").length) {
        //visualizza la schermata dello scanner
        scannerView();
    }

});


function onScanSuccess(qrCodeMessage) {

    //salva il codice nel campo di testo
    $("#input_qr-code").val(qrCodeMessage);

    //verifica il codice 
    checkCode();

}


function checkCode() {

    //cancella lo scanner
    html5QrcodeScanner.clear();

    //prende il codice inserito nel campo di testo
    code = $("#input_qr-code").val();

    $.post("/tickets/checkCode", { code }, function (data) {

        if (data.result) {
            //biglietto valido

            //visualizza la schermata di validazione
            validateView(data);

        } else {
            //biglietto non valido

            //riabilita lo scanner
            html5QrcodeScanner.render(onScanSuccess);

            //visualizza messaggi di errore
            $("#input_qr-code, .input-group").addClass("is-invalid");
            // $(".input-group").addClass("is-invalid");
            $(".invalid-feedback").removeClass("d-none");
            $(".invalid-feedback").text(data.message);

        }

    });

    //TODO: mettere la spin?
}

//visualizza la schemrata dello scanner
function scannerView() {
    //cancella il code salvato
    code = "";

    //visualizza la schermata con lo scanner
    $("#main-validate").html(scanningHtml());

    //evento click del bottone verifica
    $("#check-ticket").click(checkCode);

    if (!html5QrcodeScanner) {
        let qrbox = parseInt($("#qr-reader").width() / 2);
        html5QrcodeScanner = new Html5QrcodeScanner(
            "qr-reader", { fps: 10, qrbox });
    }
    //abilita lo scanner
    html5QrcodeScanner.render(onScanSuccess);


}


function validateView(data) {
    //carica la schermata di validazione
    $("#main-validate").html(validateHtml(data));

    //click del pulsante torna allo scanner
    $("#main-validate footer button:first-child").click(scannerView);

    //click del pulsante convalida
    $("#main-validate footer button:nth-child(2)").click(validate);
}


function validate() {

    $.post("/tickets/validate", { code }, function (data) {

        //visualizza il risultato della validazione
        $("#main-validate>div.card-body").html(`
            <div class="alert alert-${data.result ? "success" : "danger"} text-center mb-0 p-2">
                <h2 class="h5">${data.title}</h2>
                ${data.text}
            </div>`);

        //rimozione del pulsante convalida
        $("#main-validate footer button:nth-child(2)").remove();

    });

}



function scanningHtml() {

    return ` 
    <div class="card-body">

        <div class="mb-3 mx-auto" id="qr-reader"></div>

            <div class="row">
                <label for="input_qr-code" class="col-sm-5 col-md-3 col-xl-2 col-form-label">Codice del biglietto</label>

                <div class="col-sm-7 col-md-9 col-xl-10">

                    <div class="input-group">
                        <input class="form-control" id="input_qr-code" />
                        <div class="input-group-append">
                            <button class="btn btn-primary" id="check-ticket" type="button">Verifica</button>
                        </div>
                    </div>
                        
                    <div class="invalid-feedback d-none"></div>

                </div>
            </div>

    </div>`;

}

function validateHtml(data) {

    return `
    <div class="card-body">

        <div class="card border-0">
            <div class="row">

                <div class="col-md-auto align-self-center text-center">
                    <img src="/public/images/qr-tickets/${data.code}.png" alt="qr-code ${data.code}" />
                </div>

                <div class="col-md align-self-center">
                    <div class="card-body px-auto pl-md-0">
                        <h2 class="card-title h4">Biglietto ${data.code}</h2>
                        <div class="row">
                            <div class="col-md-auto">
                                <ul class="card-text list-unstyled">
                                    <li>Nome: ${data.name}</li>
                                    <li>Cognome: ${data.surname}</li>
                                </ul>
                            </div>
                            <div class="col-md">
                                <ul class="card-text list-unstyled">
                                    <li>Email: ${data.email}</li>
                                    <li>Posto prioritario (<span class="mdi mdi-wheelchair-accessibility"></span>) : ${data.priority_seat ? "SI" : "NO"}</li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>

    <footer id="action-validate" class="d-flex card-footer bg-transparent justify-content-between py-3">
        <button class="btn btn-danger">
            <span class="mdi mdi-barcode-scan"></span>
            <span class="d-none d-sm-inline">Ritorna allo Scanner</span>
            <span class="sr-only d-sm-none">Ritorna allo Scanner</span>
        </button>
        <button class="btn btn-success">
            <span class="d-none d-sm-inline">Convalida</span>
            <span class="sr-only d-sm-none">Convalida</span>
            <span class="mdi mdi-clipboard-check-outline"></span>
        </button>
    </footer>`;

}
