$(function () {
    $("form.crypt").submit(function (e) {
        //evento submit dei form di login

        let pass1 = $(this).find(".password");
        let pass2 = $(this).find(".password-check");

        if (!!pass1.val()) {

            // Crea un elemento di input che verrà usato come campo di output per la password criptata.
            let passwordCrypt = document.createElement("input");
            // Aggiungi un nuovo elemento al tuo form.
            $(this).append(passwordCrypt);
            passwordCrypt.name = "pc";
            passwordCrypt.type = "hidden"
            passwordCrypt.value = hex_sha512(pass1.val());

            // cancellazione della password per non inviarla in chiaro
            pass1.val("EH! VOLEVI!");

            if (!!pass2.val()) {
                // Crea un elemento di input che verrà usato come campo di output per la password-check criptata.
                let checkPasswordCrypt = document.createElement("input");
                // Aggiungi un nuovo elemento al tuo form.
                $(this).append(checkPasswordCrypt);
                checkPasswordCrypt.name = "cpc";
                checkPasswordCrypt.type = "hidden"
                checkPasswordCrypt.value = hex_sha512(pass2.val());
                // cancellazione della password per non inviarla in chiaro
                pass2.val("EH! VOLEVI!");
            }
        }

    });
});