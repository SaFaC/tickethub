function setCookie(name, value, dateToDie, path = "/") {
    let date = new Date();
    //crea una data allungata in giorni ( 24*60*60*1000 sono i millisecondi in un giorno)
    date.setTime(date.getTime() + (dateToDie * 24 * 60 * 60 * 1000));

    document.cookie =
        name + "=" + value + "; " +
        "expires=" + date.toUTCString() + "; " +
        "path=" + path;
    //es  "nomecookie=valorecookie; expires=Thu, 25 Dec 2020 12:00:00 UTC; path=/";
}

function getCookieValue(name, charSepVal = "-") {
    let values = false;
    //estrapola i cookie in un array dalla stringa del tipo
    //cookie1=valore1;cookie2=valore2;cookie3=valore3;
    let cookieArray = document.cookie.split(';');
    cookieArray.forEach(cookie => {
        if (cookie.indexOf(name) != -1) {
            //cookie esistente, divido il nome dai valori
            values = cookie.split("=")[1];
            //estrapolo i valori sottoforma di array
            values = values.split(charSepVal);
        }
    });

    return values;
}

function destroyCookie(name) {
    setCookie(name, "", -1);
}