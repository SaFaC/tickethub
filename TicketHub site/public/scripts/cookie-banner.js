$(function () {

    if (!getCookieValue("cookieBanner")) {

        let banner =
            `<div class="fixed-bottom alert alert-info alert-dismissible fade show" role="alert">
                <strong>Cookie Banner!</strong>
                <p>Questo sito  fa uso di cookie tecnici per poter funzionare correttamente. Proseguire con la navigazione comporta l'accettazione del loro utilizzo.
                Premere sulla x per chiudere questo messaggio e non visualizzarlo piu'.</p>
                <button type="button" class="close" data-dismiss="alert" onclick = 'setCookie("cookieBanner", 1, 36500)' >
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>`;

        $("#main-footer").append(banner);
    }

});